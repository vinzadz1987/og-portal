-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2018 at 08:53 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `communication_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `menu_access` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `userid`, `menu_access`, `created`) VALUES
(1, 1, '1,2', '2018-07-26 05:16:01'),
(3, 2, '1', '2018-07-26 04:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `acknowledges`
--

DROP TABLE IF EXISTS `acknowledges`;
CREATE TABLE `acknowledges` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `communication_id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acknowledges`
--

INSERT INTO `acknowledges` (`id`, `users`, `communication_id`, `company`, `created`) VALUES
(2, 20, 147, 1, '2018-08-08 06:06:48'),
(3, 20, 143, 1, '2018-08-08 06:09:25'),
(4, 20, 144, 1, '2018-08-08 06:09:31'),
(10, 22, 147, 1, '2018-08-08 06:33:15'),
(11, 22, 143, 1, '2018-08-08 06:33:22'),
(12, 22, 144, 1, '2018-08-08 06:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `url` varchar(200) NOT NULL,
  `icon` varchar(125) NOT NULL,
  `company` text NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `name`, `url`, `icon`, `company`, `description`, `created`) VALUES
(2, 'TestApp3', 'https://tallantasia.slack.com/', 'widgets/1531355733default.jpg', '1,2', 'edirect routing is useful when we want to inform client applications that this URL has been moved. The URL can be redirected using the following function.', '2018-07-11 01:32:29'),
(6, 'ApplicationTest1', 'https://tallantasia.slack.com/', 'widgets/1531711965Sharethis.png', '1,2,3,4', 'Test Description', '2018-07-11 06:01:54'),
(13, 'TestApp2', 'https://tallantasia.slack.com/', 'widgets/1531366887Sharethis.png', '1,2,3', 'Test', '2018-07-12 00:44:20'),
(14, 'SampleApp', 'https://tallantasia.slack.com/', 'widgets/Sharethis.png1531373320', '3', 'Test Description', '2018-07-12 05:28:40'),
(15, 'TestApp32', 'https://tallantasia.slack.com/', 'widgets/1531355733default.jpg', '1', 'test', '2018-07-16 03:33:26'),
(16, 'communication_portal', 'https://tallantasia.slack.com/', 'widgets/1532478872google_admin_logo.png', '1,2,3,4', 'Tes Description Test', '2018-07-18 00:14:18'),
(17, 'Testname2', 'https://tallantasia.slack.com/', 'widgets/1533095932company-1.png', '', 'test', '2018-07-31 06:33:21'),
(18, 'ApplicationforJob', 'https://tallantasia.slack.com/', 'widgets/1533199345company_hub.cce96176.png', '1', 'Description', '2018-07-31 06:36:19'),
(19, 'New Application', 'Link', 'widgets/1533095956company_hub.cce96176.png', '', 'Test Description', '2018-08-01 00:34:26'),
(20, 'Og Portal1', 'http://86b51cd9.ngrok.io/ogportal/applications/add', 'widgets/153354709538085198_2102278483317250_6197185397747875840_n.png', '1,2,3', 'Test', '2018-08-01 02:48:14'),
(21, 'App1', 'https://tallantasia.slack.com/', 'widgets/1533096040progress-icon.png', '3', 'test', '2018-08-01 03:15:29'),
(22, 'Ap3', 'https://tallantasia.slack.com/', 'widgets/1533096025company-corporate-growth-performance-chart-graph-success-rate-3c728f83c3d3ceee-256x256.png', '1', 'test', '2018-08-01 03:17:55'),
(23, 'App4', 'https://tallantasia.slack.com/', 'widgets/1533096067Southern_company_logo.png', '3', 'test', '2018-08-01 03:18:12'),
(24, 'Apphed', 'https://tallantasia.slack.com/', 'widgets/1533098315progress-icon.png', '1,3', 'test', '2018-08-01 03:18:36'),
(25, 'App404', 'ddsss', 'widgets/1533098291company-building-icon.png', '1,2,3', 'test', '2018-08-01 03:35:04'),
(26, 'Testname323', 'https://tallantasia.slack.com/', 'widgets/Airbnb-app-logo-png.png1533095703', '', 'ttstttsst', '2018-08-01 03:55:03'),
(27, 'Ticketing', 'https://tallantasia.slack.com/', 'widgets/256-256-0af0e35a05b3955209dd049fd0a974f5.png1533271127', '', 'Ticketing', '2018-08-03 04:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `communication_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `post_date` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `users`, `communication_id`, `comment`, `post_date`, `created`) VALUES
(1, 1, 1, 'test', '2018-7-19 14:10:15', '2018-07-19 06:10:15'),
(2, 1, 1, 'test', '2018-7-19 14:10:18', '2018-07-19 06:10:18'),
(3, 1, 1, 'comment here', '2018-7-19 15:4:11', '2018-07-19 07:04:11'),
(4, 1, 1, 'fdfgdf', '2018-7-20 9:54:2', '2018-07-20 01:54:02'),
(5, 1, 45, 'test', '2018-7-23 10:23:35', '2018-07-23 02:23:35'),
(6, 1, 49, 'Test', '2018-7-23 11:27:52', '2018-07-23 03:27:52'),
(7, 2, 136, 'test', '2018-7-23 11:43:55', '2018-08-02 08:57:32'),
(8, 2, 67, 'test', '2018-7-23 11:44:3', '2018-07-23 03:44:03'),
(9, 2, 67, 'test', '2018-7-23 11:44:5', '2018-07-23 03:44:05'),
(10, 1, 70, 'Bootstrap 4', '2018-7-23 13:46:29', '2018-07-23 05:46:30'),
(11, 1, 45, 'test', '2018-7-23 15:45:42', '2018-07-23 07:45:42'),
(12, 1, 45, 'test', '2018-7-23 15:45:45', '2018-07-23 07:45:45'),
(13, 1, 69, 'test', '2018-7-23 15:45:53', '2018-07-23 07:45:53'),
(14, 1, 69, 'test', '2018-7-23 15:45:56', '2018-07-23 07:45:56'),
(15, 2, 58, 'tewst', '2018-7-27 11:18:15', '2018-07-27 03:18:15'),
(16, 2, 70, 'test', '2018-7-31 9:14:40', '2018-07-31 01:14:40'),
(17, 2, 70, 'test', '2018-7-31 9:14:46', '2018-07-31 01:14:46'),
(18, 2, 70, 'ddddd', '2018-7-31 9:16:33', '2018-07-31 01:16:33'),
(19, 1, 136, 'ffffff', '2018-8-2 16:40:20', '2018-08-02 08:40:21'),
(20, 1, 136, 'ffffff', '2018-8-2 16:40:25', '2018-08-02 08:40:26'),
(21, 1, 45, 'test', '2018-8-2 16:49:20', '2018-08-02 08:49:21'),
(22, 1, 136, 'tsts', '2018-8-2 16:56:43', '2018-08-02 08:56:44'),
(23, 1, 136, 'tst', '2018-8-2 17:12:20', '2018-08-02 09:12:20'),
(24, 1, 136, 'now added okay', '2018-8-2 17:12:30', '2018-08-02 09:12:30'),
(25, 1, 137, 'test', '2018-8-2 17:24:3', '2018-08-02 09:24:03'),
(26, 1, 43, 'test', '2018-8-3 8:48:52', '2018-08-03 00:48:52'),
(27, 1, 142, 'test', '2018-8-7 8:17:27', '2018-08-07 00:17:27'),
(28, 25, 142, 'Hi', '2018-8-7 11:30:24', '2018-08-07 03:30:24'),
(29, 25, 142, 'By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.', '2018-8-7 11:52:37', '2018-08-07 03:52:37'),
(30, 25, 142, 'By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.', '2018-8-7 11:52:58', '2018-08-07 03:52:58'),
(31, 1, 143, 'test', '2018-8-7 14:19:11', '2018-08-07 06:19:11'),
(32, 1, 142, 'hi', '2018-8-7 15:26:32', '2018-08-07 07:26:33'),
(33, 1, 144, 'test comment here', '2018-8-7 15:38:39', '2018-08-07 07:38:39'),
(34, 1, 143, 'testcomment', '2018-8-8 8:55:46', '2018-08-08 00:55:46'),
(35, 1, 143, 'test', '2018-8-8 9:4:0', '2018-08-08 01:04:01'),
(36, 1, 143, 'test', '2018-8-8 9:4:45', '2018-08-08 01:04:45'),
(37, 1, 143, 'Noted', '2018-8-8 9:5:10', '2018-08-08 01:05:10'),
(38, 1, 142, 'test', '2018-8-8 9:5:52', '2018-08-08 01:05:52'),
(39, 1, 145, 'Comment here', '2018-8-8 9:7:11', '2018-08-08 01:07:11'),
(40, 1, 145, 'test', '2018-8-8 9:8:29', '2018-08-08 01:08:29'),
(41, 1, 146, 'ttttttttttt', '2018-8-8 9:9:22', '2018-08-08 01:09:22'),
(42, 1, 146, 'wow', '2018-8-8 9:9:29', '2018-08-08 01:09:29'),
(43, 1, 146, 'dfgfdggdfgd', '2018-8-8 9:10:51', '2018-08-08 01:10:51'),
(44, 1, 146, 'yrdy', '2018-8-8 9:11:37', '2018-08-08 01:11:38'),
(45, 1, 142, 'test', '2018-8-8 9:13:57', '2018-08-08 01:13:57'),
(46, 1, 142, 'By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.', '2018-8-8 9:14:26', '2018-08-08 01:14:26'),
(47, 1, 142, 'test', '2018-8-8 9:36:45', '2018-08-08 01:36:45'),
(48, 1, 147, 'test', '2018-8-8 9:37:47', '2018-08-08 01:37:47'),
(49, 25, 142, 'dddd', '2018-8-8 14:17:43', '2018-08-08 06:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

DROP TABLE IF EXISTS `communications`;
CREATE TABLE `communications` (
  `id` int(11) NOT NULL,
  `communication_type` int(11) NOT NULL,
  `author` varchar(500) NOT NULL,
  `start_date` date NOT NULL,
  `stop_date` date NOT NULL,
  `heading` varchar(500) NOT NULL,
  `message` text NOT NULL,
  `acknowledge_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1: not acknowledge, 2: acknowledge',
  `application` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1: draft, 2: published, 3: archived',
  `acknowledge` text NOT NULL,
  `company` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`id`, `communication_type`, `author`, `start_date`, `stop_date`, `heading`, `message`, `acknowledge_status`, `application`, `status`, `acknowledge`, `company`, `created`, `modified`) VALUES
(4, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 2, '', 0, '2018-07-20 04:26:04', '2018-08-08 02:51:34'),
(5, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 04:28:49', '2018-07-20 04:28:49'),
(6, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 2, '', 0, '2018-07-20 04:31:09', '2018-07-27 03:12:17'),
(7, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 04:40:48', '2018-07-20 04:40:48'),
(8, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 05:41:16', '2018-07-20 05:41:16'),
(9, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 05:44:54', '2018-07-20 05:44:54'),
(10, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 05:52:07', '2018-07-20 05:52:07'),
(11, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'Test Header', 1, 0, 0, '', 0, '2018-07-20 05:54:18', '2018-07-20 05:54:18'),
(12, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:02:15', '2018-07-20 06:02:15'),
(13, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:05:42', '2018-07-20 06:05:42'),
(14, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:06:25', '2018-07-20 06:06:25'),
(15, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:08:44', '2018-07-20 06:08:44'),
(16, 1, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 3, '', 0, '2018-07-20 06:09:49', '2018-07-23 03:01:53'),
(17, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:13:46', '2018-07-20 06:13:46'),
(18, 0, 'test12323t', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:16:15', '2018-07-20 06:16:15'),
(19, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:17:18', '2018-07-20 06:17:18'),
(20, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 06:17:18', '2018-07-20 06:17:18'),
(21, 0, 'test1232311111', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:19:47', '2018-07-20 08:19:47'),
(22, 0, 'test1232311111', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:20:13', '2018-07-20 08:20:13'),
(23, 0, 'test1232311111', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:20:25', '2018-07-20 08:20:25'),
(24, 0, 'test1232311111', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:21:06', '2018-07-20 08:21:06'),
(25, 1, 'test1232322222', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:22:04', '2018-07-20 08:22:04'),
(26, 1, 'test1232322222', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:22:04', '2018-07-20 08:22:04'),
(27, 1, 'test1232322222', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:22:29', '2018-07-20 08:22:29'),
(28, 0, 'test123233333', '2018-07-20', '2018-07-20', 'Test Header', 'testsssssssss', 1, 0, 0, '', 0, '2018-07-20 08:23:13', '2018-07-20 08:23:13'),
(29, 0, 'test123233333', '2018-07-20', '2018-07-20', 'Test Header', 'testsssssssss', 1, 0, 0, '', 0, '2018-07-20 08:23:13', '2018-07-20 08:23:13'),
(30, 0, 'test123234444', '2018-07-20', '2018-07-20', 'Test Header', 'testt23234234', 1, 0, 0, '', 0, '2018-07-20 08:25:29', '2018-07-20 08:25:29'),
(31, 0, 'test123234444', '2018-07-20', '2018-07-20', 'Test Header', 'testt23234234', 1, 0, 0, '', 0, '2018-07-20 08:25:29', '2018-07-20 08:25:29'),
(32, 0, 'test1232355555', '2018-07-20', '2018-07-20', 'Test Header', 'testt23234234', 1, 0, 0, '', 0, '2018-07-20 08:27:36', '2018-07-20 08:27:36'),
(33, 0, 'test1232355555', '2018-07-20', '2018-07-20', 'Test Header', 'testt23234234', 1, 0, 0, '', 0, '2018-07-20 08:27:40', '2018-07-20 08:27:40'),
(34, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'rererererererwq2weqwqweqweq', 1, 0, 0, '', 0, '2018-07-20 08:28:13', '2018-07-20 08:28:13'),
(35, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'rererererererwq2weqwqweqweq', 1, 0, 0, '', 0, '2018-07-20 08:29:15', '2018-07-20 08:29:15'),
(36, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test34234dasfsafdsf4r52343', 1, 0, 0, '', 0, '2018-07-20 08:29:35', '2018-07-20 08:29:35'),
(37, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:30:33', '2018-07-20 08:30:33'),
(38, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:34:05', '2018-07-20 08:34:05'),
(39, 1, 'test123236666', '2018-07-20', '2018-07-20', 'This is heading', 'testssssseafsdffssagdsfsf', 1, 0, 0, '', 0, '2018-07-20 08:35:34', '2018-07-20 08:35:34'),
(40, 0, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'test', 1, 0, 0, '', 0, '2018-07-20 08:42:50', '2018-07-20 08:42:50'),
(41, 0, 'test12323', '2018-07-20', '2018-07-20', 'yrdy', 'test', 1, 0, 3, '', 0, '2018-07-20 08:46:52', '2018-07-23 06:20:12'),
(42, 1, 'test', '2018-07-20', '2018-07-20', 'Test Header', 'ddfsdfsdf', 1, 0, 3, '', 0, '2018-07-20 08:47:40', '2018-07-23 06:20:02'),
(43, 1, 'Testlangnibay', '2018-08-02', '2018-07-20', 'Testinglangni', 'Testinglangni Testinglangni Testinglangni', 1, 2, 1, '', 0, '2018-07-20 08:48:58', '2018-08-02 07:01:02'),
(44, 1, 'test123238888', '2018-07-20', '2018-07-20', 'Test Header yyyydddd', 'test hdfgj jjddfgdg dgerws ', 1, 0, 1, '', 0, '2018-07-20 08:52:12', '2018-07-20 08:52:12'),
(45, 1, 'Author Me.', '2018-07-23', '2018-07-20', 'About to Published', 'Affection in your eyes.', 1, 0, 2, '', 0, '2018-07-20 08:52:16', '2018-07-23 02:08:56'),
(46, 1, 'test123238888', '2018-07-20', '2018-07-20', 'Test Header yyyydddd', 'test hdfgj jjddfgdg dgerws ', 1, 0, 3, '', 0, '2018-07-20 08:52:30', '2018-07-23 06:20:19'),
(47, 1, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'sdsdfdsfsfdf sdfsdfdsfsdfsd sdfdsfsdfsfd', 1, 0, 3, '', 0, '2018-07-20 08:57:31', '2018-07-23 01:47:13'),
(48, 1, 'Me Author2', '2018-07-23', '2018-07-20', 'This is heading', 'Test Message', 1, 0, 1, '', 0, '2018-07-20 08:59:59', '2018-07-23 03:16:48'),
(49, 1, 'Me Author', '2018-07-23', '2018-07-20', 'I want this published', 'My Test message', 1, 0, 2, '', 0, '2018-07-20 09:00:42', '2018-07-23 02:06:01'),
(50, 1, 'test12323', '2018-07-20', '2018-07-20', 'Test Header', 'ssssssss', 1, 0, 1, '', 0, '2018-07-20 09:02:16', '2018-07-20 09:02:16'),
(51, 1, 'test12323999999', '2018-07-20', '2018-07-20', 'Test Header', 'rwar', 1, 0, 3, '', 0, '2018-07-20 09:05:26', '2018-07-23 06:15:19'),
(52, 0, 'test12323', '2018-07-23', '2018-07-20', 'Test Header', 'test', 1, 0, 2, '', 0, '2018-07-20 09:06:43', '2018-07-23 02:05:18'),
(53, 1, 'test1232388888', '2018-07-20', '2018-07-20', 'test', 'test', 1, 0, 3, '', 0, '2018-07-20 09:11:14', '2018-07-23 06:20:07'),
(54, 1, 'My Name', '2018-07-23', '2018-07-20', 'This is a heading', 'Test', 1, 0, 1, '', 0, '2018-07-20 09:22:50', '2018-07-23 04:15:52'),
(55, 1, 'My Name', '2018-07-23', '2018-07-20', 'Utility', 'Export datatables', 1, 0, 1, '', 0, '2018-07-20 09:22:56', '2018-07-23 06:10:11'),
(56, 1, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:22:50', '2018-07-23 00:22:50'),
(57, 1, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:23:46', '2018-07-23 00:23:46'),
(58, 1, 'dtsds', '2018-07-24', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:30:30', '2018-07-24 09:30:08'),
(59, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:43:16', '2018-07-23 00:43:16'),
(60, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:48:22', '2018-07-23 00:48:22'),
(61, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:48:34', '2018-07-23 00:48:34'),
(62, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:51:21', '2018-07-23 00:51:21'),
(63, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 2, '', 0, '2018-07-23 00:51:50', '2018-07-23 00:51:50'),
(64, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test tessttt', 1, 0, 1, '', 0, '2018-07-23 00:55:10', '2018-07-23 00:55:10'),
(65, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-23 00:56:11', '2018-07-23 00:56:11'),
(66, 1, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'test', 1, 0, 2, '', 0, '2018-07-23 00:56:27', '2018-07-23 00:56:27'),
(67, 0, 'Save Only', '2018-07-24', '2018-07-23', 'Save Lang', 'Save', 1, 0, 2, '', 0, '2018-07-23 00:57:17', '2018-07-24 09:26:22'),
(68, 0, 'Published', '2018-07-23', '2018-07-23', 'Publish Na', 'This is Published', 1, 0, 2, '', 0, '2018-07-23 00:58:11', '2018-07-23 00:58:11'),
(69, 0, 'Me as the author', '2018-07-23', '2018-07-23', 'Update one field', 'Saving data.', 1, 0, 1, '', 0, '2018-07-23 05:23:49', '2018-07-23 05:23:49'),
(70, 0, 'Matrix', '2018-07-27', '2018-07-23', 'Form Post Link', 'SLP World', 1, 0, 1, '', 0, '2018-07-23 05:25:08', '2018-07-27 03:02:34'),
(71, 0, 'test12323', '2018-07-23', '2018-07-23', 'Test Header', 'trest', 1, 0, 1, '', 0, '2018-07-23 07:45:05', '2018-07-23 07:45:05'),
(72, 1, 'My Name', '2018-07-23', '2018-07-23', 'Test Header', 'tsss', 1, 0, 1, '', 0, '2018-07-23 07:45:22', '2018-07-23 07:45:22'),
(73, 0, 'My Name', '2018-07-26', '2018-07-26', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-26 08:32:10', '2018-07-26 08:32:10'),
(74, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 04:01:38', '2018-07-27 04:01:38'),
(75, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 05:04:11', '2018-07-27 05:04:11'),
(76, 0, 'Add Communication', '2018-07-27', '2018-07-27', 'test', 'test', 1, 0, 1, '', 0, '2018-07-27 05:04:54', '2018-07-27 05:04:54'),
(77, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 06:36:57', '2018-07-27 06:36:57'),
(78, 0, 'TestAuthor', '2018-07-27', '2018-07-27', 'TestHeader101', 'TestMessageg125', 1, 0, 1, '', 0, '2018-07-27 06:40:04', '2018-07-27 06:40:04'),
(79, 0, 'My Name', '2018-07-27', '2018-07-27', 'sdfsdf', 'test', 1, 0, 1, '', 0, '2018-07-27 06:40:57', '2018-07-27 06:40:57'),
(80, 0, 'My Name', '2018-07-27', '2018-07-27', 'sdfsdf', 'test', 1, 0, 1, '', 0, '2018-07-27 06:41:14', '2018-07-27 06:41:14'),
(81, 0, 'My Name', '2018-07-27', '2018-07-27', 'sdfsdf', 'test', 1, 0, 1, '', 0, '2018-07-27 06:41:18', '2018-07-27 06:41:18'),
(82, 0, 'My Name', '2018-07-27', '2018-07-27', 'This is heading', 'test', 1, 0, 1, '', 0, '2018-07-27 06:41:52', '2018-07-27 06:41:52'),
(83, 0, 'ddd', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 06:56:07', '2018-07-27 06:56:07'),
(84, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 06:57:20', '2018-07-27 06:57:20'),
(85, 0, 'test12323', '2018-07-27', '2018-07-27', 'test', 'tsdfdsffd', 1, 0, 1, '', 0, '2018-07-27 06:57:59', '2018-07-27 06:57:59'),
(86, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 06:58:43', '2018-07-27 06:58:43'),
(87, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'tesss', 1, 0, 1, '', 0, '2018-07-27 06:59:12', '2018-07-27 06:59:12'),
(88, 0, 'My Name', '2018-07-27', '2018-07-20', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:15:18', '2018-07-27 07:15:18'),
(89, 0, 'My Name', '2018-07-27', '2018-07-20', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:15:32', '2018-07-27 07:15:32'),
(90, 0, 'My Name', '2018-07-27', '2018-07-20', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:15:33', '2018-07-27 07:15:33'),
(91, 0, 'My Name', '2018-07-27', '2018-07-20', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:15:33', '2018-07-27 07:15:33'),
(92, 0, 'My Name', '2018-07-27', '2018-07-20', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:15:33', '2018-07-27 07:15:33'),
(93, 0, 'Author Me', '2018-07-27', '2018-07-27', 'Test hEADER', 'test', 1, 0, 1, '', 0, '2018-07-27 07:21:06', '2018-07-27 07:21:06'),
(94, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'tst', 1, 0, 1, '', 0, '2018-07-27 07:34:19', '2018-07-27 07:34:19'),
(95, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:40:49', '2018-07-27 07:40:49'),
(96, 0, 'AkoangAuthor', '2018-07-27', '2018-07-27', 'Test Header', 'dddddd test', 1, 0, 1, '', 0, '2018-07-27 07:41:36', '2018-07-27 07:41:36'),
(97, 0, 'Michelle', '2018-07-27', '2018-07-27', 'tsddd', 'tstst', 1, 0, 1, '', 0, '2018-07-27 07:42:35', '2018-07-27 07:42:35'),
(98, 0, 'Michelle', '2018-07-27', '2018-07-27', 'tsddd', 'tstst', 1, 0, 1, '', 0, '2018-07-27 07:42:54', '2018-07-27 07:42:54'),
(99, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:00', '2018-07-27 07:44:00'),
(100, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:14', '2018-07-27 07:44:14'),
(101, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:17', '2018-07-27 07:44:17'),
(102, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:35', '2018-07-27 07:44:35'),
(103, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:37', '2018-07-27 07:44:37'),
(104, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:39', '2018-07-27 07:44:39'),
(105, 0, 'John', '2018-07-27', '2018-07-27', 'TestHeading', 'TestHeading TestHeading TestHeading', 1, 0, 1, '', 0, '2018-07-27 07:44:41', '2018-07-27 07:44:41'),
(106, 0, 'author', '2018-07-27', '2018-07-27', 'sdfsfdfsdfdf', 'sdfdsfsfffsdf', 1, 0, 1, '', 0, '2018-07-27 07:46:31', '2018-07-27 07:46:31'),
(107, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'tsstt Message me', 1, 0, 1, '', 0, '2018-07-27 07:49:20', '2018-07-27 07:49:20'),
(108, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:50:41', '2018-07-27 07:50:41'),
(109, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:50:55', '2018-07-27 07:50:55'),
(110, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:51:36', '2018-07-27 07:51:36'),
(111, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:51:55', '2018-07-27 07:51:55'),
(112, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:51:58', '2018-07-27 07:51:58'),
(113, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:52:00', '2018-07-27 07:52:00'),
(114, 0, 'My Name', '2018-07-27', '2018-07-27', 'Testtest', 'Hello', 1, 0, 1, '', 0, '2018-07-27 07:52:03', '2018-07-27 07:52:03'),
(115, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 07:53:02', '2018-07-27 07:53:02'),
(116, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'dddd', 1, 0, 1, '', 0, '2018-07-27 07:57:18', '2018-07-27 07:57:18'),
(117, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'dddd', 1, 0, 1, '', 0, '2018-07-27 07:57:38', '2018-07-27 07:57:38'),
(118, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-27 08:07:28', '2018-07-27 08:07:28'),
(119, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'dddd ddd test test Test', 1, 0, 1, '', 0, '2018-07-27 08:11:16', '2018-07-27 08:11:16'),
(120, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'dddd ddd test test Test', 1, 0, 1, '', 0, '2018-07-27 08:11:41', '2018-07-27 08:11:41'),
(121, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'dddd ddd test test Test', 1, 0, 1, '', 0, '2018-07-27 08:12:05', '2018-07-27 08:12:05'),
(122, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'Test Test...', 1, 0, 1, '', 0, '2018-07-27 09:27:21', '2018-07-27 09:27:21'),
(123, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'Test Test...', 1, 0, 1, '', 0, '2018-07-27 09:27:29', '2018-07-27 09:27:29'),
(124, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'Test Test...', 1, 0, 2, '', 0, '2018-07-27 09:28:31', '2018-07-27 09:28:31'),
(125, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'Test Test...', 1, 0, 2, '', 0, '2018-07-27 09:28:36', '2018-07-27 09:28:36'),
(126, 0, 'test12323', '2018-07-27', '2018-07-27', 'Test Header', 'Test Test...', 1, 0, 2, '', 0, '2018-07-27 09:28:38', '2018-07-27 09:28:38'),
(127, 0, 'My Name', '2018-07-30', '2018-07-30', 'This is heading', 'test', 1, 0, 1, '', 0, '2018-07-30 00:27:42', '2018-07-30 00:27:42'),
(128, 0, 'test12323', '2018-07-30', '2018-07-30', 'Test Header', 'tssdfsdf', 1, 0, 1, '', 0, '2018-07-30 01:17:06', '2018-07-30 01:17:06'),
(129, 0, 'test12323', '2018-07-30', '2018-07-30', 'Test Headerttttttt', 'ssfdfsdfds', 1, 0, 1, '', 0, '2018-07-30 03:43:06', '2018-07-30 03:43:06'),
(130, 0, 'Test', '2018-07-31', '2018-07-31', 'Test Header', 'test', 1, 0, 1, '', 0, '2018-07-30 06:01:59', '2018-07-31 01:15:33'),
(131, 0, 'Author', '2018-07-31', '2018-07-31', 'Heading', 'Message', 1, 0, 1, '', 0, '2018-07-31 01:30:41', '2018-07-31 01:30:41'),
(132, 0, 'Author', '2018-07-31', '2018-07-31', 'Heading', 'Message', 1, 0, 1, '', 0, '2018-07-31 01:31:23', '2018-07-31 01:31:23'),
(133, 0, 'test12323', '2018-07-31', '2018-07-31', 'This is heading', 'test', 1, 0, 1, '', 0, '2018-07-31 01:31:39', '2018-07-31 01:31:39'),
(134, 0, 'Author Me', '2018-08-06', '2018-08-02', 'Test Header', 'test', 1, 2, 1, '', 0, '2018-08-02 06:23:19', '2018-08-06 08:45:10'),
(135, 0, 'Author Me', '2018-08-06', '2018-08-02', 'Test Header', 'test', 1, 2, 1, '', 0, '2018-08-02 06:23:50', '2018-08-06 07:16:02'),
(136, 0, 'Doyle Jabilles', '2018-08-06', '2018-08-02', 'Help Me with this!', 'How to do it.', 1, 2, 3, '1,2,3', 0, '2018-08-02 06:42:35', '2018-08-06 07:34:46'),
(137, 0, 'test12323', '2018-08-06', '2018-08-02', 'Test Header', 'dddddd', 1, 2, 2, '', 0, '2018-08-02 06:42:59', '2018-08-06 07:35:12'),
(138, 0, 'ddd', '2018-08-02', '2018-08-02', 'This is heading', 'dddfdfd', 1, 6, 1, '', 0, '2018-08-02 06:43:26', '2018-08-02 06:43:26'),
(139, 0, 'test12323', '2018-08-02', '2018-08-02', 'This is heading', 'test', 1, 15, 1, '', 0, '2018-08-02 07:14:41', '2018-08-02 07:14:41'),
(140, 0, 'test12323', '2018-08-06', '2018-08-06', 'Test Header', 'testt', 1, 14, 1, '', 0, '2018-08-06 09:13:38', '2018-08-06 09:13:38'),
(141, 0, 'test12323', '2018-08-06', '2018-08-06', 'This is heading', 'ddddd', 1, 2, 1, '', 2, '2018-08-06 09:27:29', '2018-08-06 09:27:29'),
(142, 0, 'My Name', '2018-08-07', '2018-08-06', 'Cache', 'By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.', 1, 2, 3, '', 1, '2018-08-06 09:34:04', '2018-08-07 03:25:14'),
(143, 0, 'Im the author', '2018-08-07', '2018-08-07', 'Deprecations', 'Note: This is a hybrid layout that works with both Bootstrap 3 & 4 versions. Please check out the tutorial on Bootstrap Modals to learn how to customize this layout further.', 1, 2, 2, '20,22', 1, '2018-08-07 02:55:59', '2018-08-08 06:33:22'),
(144, 0, 'Doyle', '2018-08-07', '2018-08-07', 'Environment', 'Environment', 1, 2, 2, '20,22', 1, '2018-08-07 07:38:10', '2018-08-08 06:33:27'),
(145, 0, 'History', '2018-08-07', '2018-08-07', 'History', 'History', 1, 2, 2, '', 1, '2018-08-07 07:51:02', '2018-08-08 06:35:54'),
(146, 0, 'Portal', '2018-08-08', '2018-08-23', 'Test Header', 'test', 1, 6, 2, '', 1, '2018-08-08 01:09:12', '2018-08-08 06:36:00'),
(147, 0, 'Me', '2018-08-08', '2018-08-08', 'Include', 'Include', 1, 2, 2, '20,22', 1, '2018-08-08 01:22:09', '2018-08-08 06:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `userid`, `name`, `description`, `created`, `modified`) VALUES
(1, 1, 'TestCompany1', 'TestCompany', '2018-07-26 02:14:13', '2018-07-26 02:14:13'),
(2, 2, 'Company2', 'Company', '2018-07-26 02:17:32', '2018-07-26 02:17:32'),
(3, 1, 'CompanyTest', 'Test', '2018-07-26 05:47:04', '2018-07-26 05:47:04'),
(4, 2, 'Og Portal', 'Og Portal', '2018-07-31 04:23:56', '2018-07-31 04:23:56'),
(5, 0, 'TestApp3', 'test', '2018-08-01 05:28:18', '2018-08-01 05:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `users` int(11) NOT NULL,
  `communication_id` int(11) NOT NULL,
  `date_post` varchar(225) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `users`, `communication_id`, `date_post`, `created`) VALUES
(15, 1, 143, '2018-8-7 17:30:28', '2018-08-07 09:30:28'),
(16, 1, 144, '2018-8-7 17:30:39', '2018-08-07 09:30:40'),
(17, 1, 145, '2018-8-7 17:30:45', '2018-08-07 09:30:45'),
(19, 1, 142, '2018-8-8 9:3:45', '2018-08-08 01:03:45'),
(21, 20, 142, '2018-8-8 14:13:58', '2018-08-08 06:13:58'),
(22, 25, 142, '2018-8-8 14:17:37', '2018-08-08 06:17:37');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `sub` text NOT NULL,
  `controller` varchar(500) NOT NULL,
  `action` varchar(225) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `category` int(11) NOT NULL COMMENT '1: Admins, 2: Users',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `description`, `sub`, `controller`, `action`, `icon`, `category`, `created`) VALUES
(5, 'Companies', 'Company', '', 'Companies', 'index', 'fas fa-building', 1, '2018-08-01 08:36:58'),
(6, 'Users', 'Users', '', 'Users', 'index', 'fas fa-users', 1, '2018-08-01 08:37:01'),
(7, 'Applications', 'test', '', 'Applications', 'index', 'mdi mdi-account-check', 1, '2018-08-01 08:37:03'),
(8, 'Dashboard', 'Dashboard', '', 'Applications', 'dashboard', 'mdi mdi-view-dashboard', 2, '2018-08-02 03:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `comment`) VALUES
(1, 'Admin', 'this super admin of this platform'),
(2, 'User', 'this staff managing this platform'),
(3, 'Super Admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `submenus`
--

DROP TABLE IF EXISTS `submenus`;
CREATE TABLE `submenus` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `controller` varchar(500) NOT NULL,
  `action` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submenus`
--

INSERT INTO `submenus` (`id`, `name`, `description`, `controller`, `action`, `created`) VALUES
(1, 'List', 'List of Communication', 'Communications', 'lists', '2018-07-25 04:47:10'),
(2, 'Archived', 'Archived of communications', 'Communications', 'archived', '2018-07-25 04:47:15'),
(3, 'List', 'Menu menu list', 'Menus', 'index', '2018-07-25 04:47:17'),
(4, 'Access', 'Access', 'Users', 'access', '2018-07-26 00:37:12'),
(5, 'Submenu', 'submenus', 'Submenus', 'index', '2018-07-26 00:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `firstname` varchar(225) NOT NULL,
  `lastname` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `company_role` int(11) NOT NULL COMMENT '0: Admin, 1: Staff',
  `company` int(11) NOT NULL,
  `category` int(11) NOT NULL COMMENT '0: og user, 1: company user',
  `access` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `email`, `password`, `role`, `company_role`, `company`, `category`, `access`, `created`, `modified`) VALUES
(1, 'Admin Name', 'Admin1', 'Name', 'admin1@test.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', 'User', 0, 1, 0, 1, '2018-07-03 23:59:38', '2018-07-30 08:20:22'),
(2, 'Admin Name', 'Admin2', 'Name', 'testme@test.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', 'Admin', 0, 0, 0, 2, '2018-07-04 01:32:30', '2018-07-04 01:32:30'),
(3, 'User Name', 'User', 'Name', 'user1@test.com', '$2y$10$gbZiE/L3DAri2w.ZgbFdsOY6K.iNyI/frdnXs7Gx3OhzTthbzFw5m', 'User', 0, 3, 0, 0, '2018-07-04 01:54:41', '2018-08-01 09:47:52'),
(4, 'Admin Name', 'Alvin', 'Ado', 'vinzadz1987@gmail.com', '$2y$10$qMZCC2VkKEnJkGunHB98/Ohq86skQG5CvFPXekmROfTj0xTN3c4xa', 'Admin', 0, 0, 0, 0, '2018-07-04 02:05:48', '2018-07-30 03:23:18'),
(5, 'Admin Name', 'dsdsf', 'dfs', 'alvin@test.com', '$2y$10$kh1FQP50GDmfvn9kDxFmsO2P2TtnP.j97/yRA7GKPY5qJpmtAfUOq', 'Admin', 0, 0, 0, 0, '2018-07-06 00:07:22', '2018-07-30 04:24:50'),
(6, '', 'Sheyee', 'Wilson', 'user123@test.com', '$2y$10$ebWLevsxEUUZkX1cG4zlu.eGW2JvC6rFLmkc./L1h4BT5U5xWSbHW', 'User', 0, 3, 0, 0, '2018-07-24 01:17:04', '2018-07-30 08:28:42'),
(16, '', 'Alvin', 'ddd', 'testmedddddd@test.com', '$2y$10$xALPS5ANQnoGBLxPRDDODOwOvtEtFuRYcJPHhc1BJVvrZnUwJDW4C', 'User', 0, 2, 0, 0, '2018-07-30 04:20:09', '2018-08-06 09:05:16'),
(17, '', 'Amor', 'Power', 'testmed@test.com', '$2y$10$6RImXNyFHwnRbdzlA12NKOrftSHL9Ejnk2APLf8yKkS3vL3Y7xVWS', 'User', 1, 2, 0, 0, '2018-07-30 04:24:21', '2018-07-31 06:08:06'),
(18, '', 'test', 'test', 'admin122@test.com', '$2y$10$PxylAT9Byvq7as3ofnEC9u5f9s6lVhEPEK1es8n3hO0lVpF8TdbTm', 'User', 0, 0, 0, 0, '2018-07-30 06:03:55', '2018-07-30 06:03:55'),
(19, '', 'Sino', 'Dito', 'admin1222@test.com', '$2y$10$fW9yYXDrHF0fyAtRFSoy8umJYOOWM.4YEk6D4Txu8j1ye6dhjztcW', 'User', 0, 4, 0, 0, '2018-07-30 06:07:06', '2018-07-30 08:47:02'),
(20, '', 'Alvin2', 'Ado2', 'ado2@test.com', '$2y$10$AUiH02EjzHGrkSkpb.q2D.u8.X6RV0e.qbK8spEczrtLvXY1FW/fO', 'User', 1, 1, 0, 1, '2018-07-30 06:08:40', '2018-08-07 02:01:23'),
(21, '', 'Alvin', 'Ado', 'admidddn1@test.com', '$2y$10$MVYV1yaFODLSU1sVFGAkI.dpvf2HPi.arw7fRdHrf3TxMfzdLv5N2', 'User', 0, 0, 0, 0, '2018-07-30 06:09:40', '2018-07-30 06:09:40'),
(22, '', 'Habol', 'Ganyan', 'admin1ddd@test.com', '$2y$10$c8lTr0/j9EX/q/oAUhXw7uYIWXvLFJOgThpX5SeW.XCIvDsZ4Rebu', 'User', 1, 1, 0, 0, '2018-07-30 06:16:34', '2018-08-08 06:30:19'),
(23, '', 'Jason', 'Tesuluna', 'jason@tesulana.com', '$2y$10$86v9u87gNBK48LTAjO377OY4Tb1Rf0fIfZs9.TO0zlX379l4zwlhW', 'User', 0, 1, 0, 0, '2018-07-30 06:27:35', '2018-07-30 06:27:35'),
(24, '', 'Niel', 'Rnace', 'niel@gmail.cm', '$2y$10$0KWfD3NuDMVrlEwEJndJre5JBbdOheK2yO2ai7uBPjYjNsGnaGmEm', 'User', 1, 2, 0, 0, '2018-07-30 06:28:22', '2018-07-31 06:02:02'),
(25, '', 'Brett', 'Quinn', 'brettsample@test.com', '$2y$10$pYixPv7x7F.z7It0mJnYgeViivH723U2K5yLyQX26ym/RkxuFzQey', 'User', 0, 2, 0, 0, '2018-08-03 06:09:14', '2018-08-08 06:25:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acknowledges`
--
ALTER TABLE `acknowledges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submenus`
--
ALTER TABLE `submenus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `acknowledges`
--
ALTER TABLE `acknowledges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `submenus`
--
ALTER TABLE `submenus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
