<div class="card">
	<div class="card-body">
		<h3  class="card-title"><b>Applications</b></h3>
		<div style="padding: 5px">
			<?php
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Add Application',
					[
						'controller' => 'Applications',
						'action' => 'add'
					],
					[ 
						'escape' => false,
						'class' => 'btn btn-primary'
					]
				)
			?>
		</div>
		<div class="table-responsive">
			<table id="applicationsTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Application Name</th>
						<th>URL Link</th>
						<th>Assigned</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($applications as $application): ?>
						<tr>
							<td><?= h($application->name) ?></td>
							<td><a href=""><?= h($application->url) ?></a></td>
							<td>
							<?php
								$cnt_comp = explode(',', h($application->company));
								$countcomp = (!empty(h($application->company)))? count($cnt_comp) : 0;
								echo $this->Html->link(
										'<i class="fa fa-building"></i> <b>'.$countcomp.'</b> Company',
									[
										'controller' => 'Applications',
										'action' => 'company',
										h($application->id)
									],
									[ 
										'escape' => false
									]
								)
							?>
							</td>
							<td>
							<?= $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), ['action' => 'edit', $application->id], ['escape' => false,'class' => 'btn-xs btn btn-primary btn-md', 'escape' => false]) ?>
							<?= $this->Form->postLink(__('<i class="fa fa-archive"></i> Archive'), ['action' => 'delete', $application->id], ['confirm' => __('Are you sure you want to delete # {0}?', $application->id),'escape' => false, 'class' => 'btn-xs btn btn-danger btn-md', 'escape' => false]) ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>