<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	#sortable li { margin: 3px 3px 100px 0; padding: 1px; float: left; width: 200px; height: 100px; font-size: 4em; text-align: center; }
	.ui-tooltip, .arrow:after {
		background: black;
		border: 2px solid white;
	}
	.ui-tooltip {
		padding: 10px 20px;
		color: white;
		border-radius: 20px;
		font: bold 14px "Helvetica Neue", Sans-Serif;
		box-shadow: 0 0 7px black;
	}
	.arrow {
		width: 70px;
		height: 16px;
		overflow: hidden;
		position: absolute;
		left: 50%;
		margin-left: -35px;
		bottom: -16px;
	}
	.arrow.top {
		top: -16px;
		bottom: auto;
	}
	.arrow.left {
		left: 20%;
	}
	.arrow:after {
		content: "";
		position: absolute;
		left: 20px;
		top: -20px;
		width: 25px;
		height: 25px;
		box-shadow: 6px 5px 9px -9px black;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	}
	.arrow.top:after {
		bottom: -20px;
		top: auto;
	}

	/* Pagination
	/*================================================*/
	ul.pagination {
		margin: 0;
	}

	ul.pagination li {
		list-style-type: none;
		font-size: 1.2em;
		display: inline;
	}

	ul.pagination li a {
		display: block;
		float: left;
		padding: 2px 8px;
		margin-right: 3px;
		color: #777;
	}

	ul.pagination li span {
		float: left; 
		margin-right: 4px;
	}

	ul.pagination li a.page {
		background-color: #9FA19F;
		color: #ddd;
		font-weight: bold;
	}

	ul.pagination li a.page:hover {
		text-decoration: none;
		color: #fff;
	}

	ul.pagination li.selected a.page {
		background-color: #545C5F;
		color: #fff;
	}

	ul.pagination li.disabled a {
		color: #ddd;
		cursor: default;
	}
</style>
<div class="row">
	<ul id="sortable">
	<?php foreach ($applications as $application): ?>
		<li class="ui-state-default">
			<?php echo $this->Html->link( '
				<div class="card card-hover" title="'.h($application->description).'">
					<div class="box bg-cyan text-center">
						<h1 class="font-light text-white"><img src="'.$this->request->getAttribute('webroot').'/'.h($application->icon).'" alt="widgets" class="rounded-circle" style="width: 100px; height: 106px"><i class="mdi '.h($application->icon).'"></i></h1>
						<h6 class="text-white">'.h($application->name).'</h6>
					</div>
				</div>',
				h($application->url),
				['escape' => false, 'target' => '_blank'] 
			) 
			?>
		</li>
	<?php endforeach; ?>
	</ul>
</div>

<div class="modal fade bd-example-modal-lg" id="communicatioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<?= $this->Flash->render() ?>
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel"><?= $communication_type[$communicationlist->toArray()[0]['communication_type']] ?></h4>
				<p class="float-right"><?= $this->Paginator->counter(['format' => __('Message {{page}} of {{pages}} total')]) ?></p>
			</div>
			<div class="modal-body">
				<?php foreach ($communicationlist as $communication): ?>
				<div class="d-flex flex-row comment-row m-t-0">
					<div class="p-2"><img src="<?php echo $this->request->getAttribute('webroot').'img/default.jpg' ?>" alt="widgets" class="rounded-circle" style="width: 50px; height: 50px"></div>
					<div class="comment-text w-100">
						<h6 class="font-medium">Author: <b><?= h($communication->author) ?></b></h6>
						<span class="m-b-15 d-block"><?= h($communication->message) ?></span>
						<div class="comment-footer">
							<span class="text-muted float-right"><?= h($communication->created) ?></span> 
						</div>
					</div>
				</div>
				<div class="paginator">
					<ul class="pagination">
						<?= $this->Paginator->prev('< ' . __('previous')) ?>
						<?= $this->Paginator->next(__('next') . ' >') ?>
					</ul>
				</div>
			</div>
			<div class="modal-footer">
				<span><input type="checkbox" name="checkAcknowledge" class="checkAcknowledge" data-id="<?= $communication->id ?>"> Acknowledge</span>
				<button type="button" class="btn btn-danger acknowledgeme btnAcknowledge<?= $communication->id ?>" data-id="<?= $communication->id ?>" style="display: none">Ok</button>
			</div>
			<?php endforeach; ?>

			<?php echo $this->Form->create(null,['url' => BASE_URL.'applications/acknowledge', 'id' => 'acknowledge']); ?>
				<?= $this->Form->hidden('id', ['value' => $communication->id, 'id' => 'acknowledgeid']); ?>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

<?=$this->Form->button('<i class="fas fa-eye"></i> View', ['type' => 'button', 'class' => 'btn btn-cyan btn-sm communicationProblem hide', 'data-toggle' => 'modal', 'data-target' => '#communicatioModal', 'data-backdrop' => 'static', 'data-keyboard' => 'false']) ?>
