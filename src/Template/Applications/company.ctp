<div class="card">
	<div class="card-body">
		<h3  class="card-title"><b><?= ucfirst($application_name) ?> Application - Assign Company</b></h3>
		<div class="p5">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#assign_company">
			<i class="fa fa-plus"></i> Assign Company
		</button>
		<?php
			echo $this->Html->link(
				'<i class="fa fa-undo"></i> Back',
			[
				'controller' => 'Applications',
				'action' => 'index'
			],
			[ 
				'escape' => false,
				'class' => 'btn btn-warning float-right'
			]
			)
		?>
		</div>
		<div class="table-responsive">
			<table id="companyApplicationTable" class="table">
				<thead>
					<tr>
						<th>Company Name</th>
						<th>Created</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($companies as $company): ?>
						<tr>
							<td><?= h($company->name) ?></td>
							<td><?= h($company->created) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="assign_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel"><?= ucfirst($application_name) ?> Assign Company</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?= $this->Form->create(null,['url' => ['controller' => 'Applications','action' => 'assigncompany']]) ?>
					<?= $this->Form->control('id',['type' => 'hidden', 'value' => $id]) ?>
					<?= $this->Form->control('company',['type' => 'hidden', 'value' => $applications_company]) ?>
					<?= $this->Form->control('company_select',[
						'options' => $unassign_company,
						'empty' => 'Please Select',
						'class' => 'form-control select2',
						'multiple' => 'multiple',
						'label' => 'Company'
					]) ?>
			</div>
			<div class="modal-footer">
				<?= $this->Form->button(__('<i class="fas fa-check"></i> Assign'), ['class' => 'btn btn-primary btn-md', 'escape' => false]) ?>
				<?= $this->Form->end() ?>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
			</div>
		</div>
	</div>
</div>