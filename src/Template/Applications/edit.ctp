<style type="text/css">
  .btn-file {
    position: relative;
    overflow: hidden;
  }
  .btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
  }
</style>
<div class="users form col-md-8 large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
    <?= $this->Form->create($application, ['type' => 'file']) ?>
    <fieldset>
        <legend><h3><?= __('Update Application') ?></h3></legend>
		<?php echo $this->Form->control('name', ['label' => 'Title', 'class' => 'form-control']) ?>
		<?= $this->Form->control('url', ['class' => 'form-control', 'label' => 'URL Link']) ?>
		<small class="text-muted">(e.g: http://www.example.com)</small>
    <div class="input-group">
      <label class="input-group-btn">
        <span class="btn btn-primary">
        Application Image &hellip; <?= $this->Form->control('icon', ['type' => 'file', 'accept' => 'image/*', 'style' => 'display: none;','label' => false]) ?>
        </span>
      </label>
      <input type="text" class="form-control" readonly>
    </div>
		<small class="text-muted">File exceeds the maximum allowed size (2MB)</small>
		<?= $this->Form->control('description', ['class' => 'form-control', 'type' => 'textarea']) ?>
    </fieldset>
    <hr>
	<?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-primary','name' => 'save', 'escape' => false]) ?>
	<?php
		echo $this->Html->link(
			'<i class="fa fa-undo"></i> Cancel',
			[
				'action' => 'index'
			],
			[
				'escape' => false,
				'class' => 'btn btn-danger btn-md'
			]
		)
	?>
    <?= $this->Form->end() ?>
</div>
<?= $this->Html->script([
  'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'
]) ?>
<script>
    $(function() {

      // We can attach the `fileselect` event to all file inputs on the page
      $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });

      // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {

              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;

              if( input.length ) {
                  input.val(log);
              } else {
                  if( log ) alert(log);
              }

          });
      });

    });
  </script>
