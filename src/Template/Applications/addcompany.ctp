<div class="users form col-md-8 large-9 medium-8 columns content" style="background-color:white; padding: 20px ">
    <?= $this->Form->create($application, ['type' => 'file']) ?>
    <fieldset>
        <legend><h3><?= __('Assign Company') ?></h3></legend>
		<?php echo $this->Form->control('name', ['label' => 'Title', 'class' => 'form-control']) ?>
    </fieldset>
    <hr>
	<?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
	<?php
		echo $this->Html->link(
			'<i class="fa fa-undo"></i> Cancel',
			[
				'action' => 'index'
			],
			[ 
				'escape' => false,
				'class' => 'btn btn-danger btn-md'
			]
		)
	?>
    <?= $this->Form->end() ?>
</div>

