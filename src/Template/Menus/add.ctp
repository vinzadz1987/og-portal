<style type="text/css">
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
	    background-color: #2255A4 !important;
	    border: 1px solid #aaa !important;
	    border-radius: 4px !important;
	    cursor: default !important;
	    float: left !important;
	    margin-right: 5px !important;
	    margin-top: 5px !important;
	    padding: 0 5px !important;
	}
</style>
<div class="card">
	<div class="card-body table-responsive">
		<div class="menus form large-9 medium-8 columns content">
			<?= $this->Form->create($menu) ?>
			<fieldset>
				<legend><?= __('Add Menu') ?></legend>
				<?php
					echo $this->Form->control('name',['class' => 'form-control']);
					echo $this->Form->control('description',['class' => 'form-control']); 
				?>
				<?= $this->Form->control('submenu', [
					'options' => $select_submenus,
					'class' => ['select2 form-control m-t-15'],
					'multiple' => 'multiple'
				]) ?> 
				<?php	
					echo $this->Form->control('sub',['class' => 'form-control']);
					echo $this->Form->control('controller',['class' => 'form-control']);
					echo $this->Form->control('action',['class' => 'form-control']);
					echo $this->Form->control('icon',['class' => 'form-control']);
				?>
				</fieldset>
			<hr>
			<?= $this->Form->button(__('Save'),['class' => 'btn btn-primary']) ?>
			<?= $this->Html->link(__('Cancel'), ['action' => 'index'],['class' => 'btn btn-danger']) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>

