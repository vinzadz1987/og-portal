<div class="card">
    <div class="card-body table-responsive">
		<div class="menus index medium-12 large-9 medium-8 columns content">
		    <h3><?= __('Menus') ?></h3>
		    <div style='padding:10px'>
		    	<?= $this->Html->link(__('New Menu'), ['action' => 'add'], ['class' => 'btn btn-primary']) ?>
		    	<?= $this->Html->link(__('Submenu'), ['controller' => 'Submenus','action' => 'index'], ['class' => 'btn btn-info']) ?>
		    </div>
		    <table class="table">
		        <thead>
		            <tr>
		                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('sub_menu') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('controller') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('action') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('icon') ?></th>
		                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
		                <th scope="col" class="actions"><?= __('Actions') ?></th>
		            </tr>
		        </thead>
		        <tbody>
		            <?php foreach ($menus as $menu): ?>
		            <tr>
		                <td><?= $this->Number->format($menu->id) ?></td>
		                <td><?= h($menu->name) ?></td>
		                <td><?= h($menu->description) ?></td>
		                <td>
	                		<?php 
								$sub_menu = explode(',',h($menu->sub)); 
								$sbmnus = "";
								for($i=0; $i < sizeof($sub_menu); $i++) {
									if($sub_menu[$i] !== "") {
										echo '<span class="badge badge-pill badge-info">'.$submenus[$sub_menu[$i]]->name.'</span>';
									}
								}
							?>
						</td>
		                <td><?= h($menu->controller) ?></td>
		                <td><?= h($menu->action) ?></td>
		                <td><i class="mdi <?= h($menu->icon) ?>"></i></td>
		                <td><?= h($menu->created) ?></td>
		                <td class="actions">
		                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menu->id]) ?>
		                    <?= $this->Form->postLink(__('Archived'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menu->id)]) ?>
		                </td>
		            </tr>
		            <?php endforeach; ?>
		        </tbody>
		    </table>
		    <div class="paginator">
		        <ul class="pagination">
		            <?= $this->Paginator->first('<< ' . __('first')) ?>
		            <?= $this->Paginator->prev('< ' . __('previous')) ?>
		            <?= $this->Paginator->numbers() ?>
		            <?= $this->Paginator->next(__('next') . ' >') ?>
		            <?= $this->Paginator->last(__('last') . ' >>') ?>
		        </ul>
		        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
		    </div>
		</div>
	</div>
</div>
