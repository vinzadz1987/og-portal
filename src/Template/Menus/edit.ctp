
<div class="card">
    <div class="card-body table-responsive">
        <div class="menus form large-9 medium-8 columns content">
            <?= $this->Form->create($menu) ?>
            <fieldset>
                <legend><?= __('Edit Menu') ?></legend>
                <?php
                    echo $this->Form->control('name',['class' => 'form-control']);
                    echo $this->Form->control('description',['class' => 'form-control']);
                    echo $this->Form->control('sub',['class' => 'form-control']);
                    echo $this->Form->control('controller',['class' => 'form-control']);
                    echo $this->Form->control('action',['class' => 'form-control']);
                    echo $this->Form->control('icon',['class' => 'form-control']);
                ?>
                </fieldset>
            <hr>
            <?= $this->Form->button(__('Edit'),['class' => 'btn btn-primary']) ?>
            <?= $this->Html->link(__('Cancel'), ['action' => 'index'],['class' => 'btn btn-danger']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

