<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Outsourced Global Portal';
?>
<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html dir="ltr">
<head>
    <?= $this->Html->charset() ?>
    <?= $this->Html->meta([
        'http-equiv' => 'X-UA-Compatible',
        'content' => 'IE=edge'
    ]) ?>
    <!-- Tell the browser to be responsive to screen width -->
    <?= $this->Html->meta(
        'viewport',
        'width=device-width, initial-scale=1'
    ) ?>
    <?= $this->Html->meta(
        'description'
    ) ?>
    <?= $this->Html->meta(
        'author'
    ) ?>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>Outsourced Global Portal</title>
    <!-- Custom CSS -->
    <?= $this->Html->css([
    	'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css',
    	'style.min',
    	'comm_home',
    	'datatables.net-bs4/css/dataTables.bootstrap4',
    	'bootstrap-datepicker/dist/css/bootstrap-datepicker.min',
    	'quill/dist/quill.snow',
    	'toastr/build/toastr.min',
    	'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
    	'select2/dist/css/select2.min',
    	'https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css',
    	'icons/font-awesome/css/fontawesome-all'
    ]) ?>
</head>
<style type="text/css">
	@keyframes placeHolderShimmer{
		0%{
			background-position: -468px 0
		}
		100%{
			background-position: 468px 0
		}
	}
	.loadingAnimated {
		animation-duration: 1s;
		animation-fill-mode: forwards;
		animation-iteration-count: infinite;
		animation-name: placeHolderShimmer;
		animation-timing-function: linear;
		background: #f6f7f8;
		background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
		background-size: 800px 104px;
		height: 96px;
		position: relative;
	}
	select {
		height: 35px !important;
	}
	a {
		text-decoration: none !important;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		background-color: #2255A4 !important;
		border: 1px solid #aaa !important;
		border-radius: 4px !important;
		cursor: default !important;
		float: left !important;
		margin-right: 5px !important;
		margin-top: 5px !important;
		padding: 0 5px !important;
	}
</style>
<body>
    
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
	    <!-- ============================================================== -->
	    <!-- Topbar header - style you can find in pages.scss -->
	    <!-- ============================================================== -->
	    <?php echo $this->element('home/header') ?>
	    <!-- ============================================================== -->
	    <!-- End Topbar header -->
	    <!-- ============================================================== -->
	    <!-- ============================================================== -->
	    <!-- Left Sidebar - style you can find in sidebar.scss  -->
	    <!-- ============================================================== -->
	    <?php echo $this->element('home/sidebar') ?>
	    <!-- ============================================================== -->
	    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
	    <!-- ============================================================== -->
	    <!-- ============================================================== -->
	    <!-- Page wrapper  -->
	    <!-- ============================================================== -->
	    <div class="page-wrapper">
	        <!-- ============================================================== -->
	        <!-- Bread crumb and right sidebar toggle -->
	        <!-- ============================================================== -->
	        <?= $this->Flash->render() ?>
	         <div class="page-breadcrumb">
	            <div class="row">
	                <div class="col-12 d-flex no-block align-items-center">
	                    <h4 class="page-title"><?= isset($page_title)? $page_title : '' ?></h4>
	                    <!-- <div class="ml-auto text-right">
	                        <nav aria-label="breadcrumb">
	                            <ol class="breadcrumb">
	                                <li class="breadcrumb-item"><a href="#">Home</a></li>
	                                <li class="breadcrumb-item active" aria-current="page">Library</li>
	                            </ol>
	                        </nav>
	                    </div> -->
	                </div>
	            </div>
	        </div>
	        <!-- ============================================================== -->
	        <!-- End Bread crumb and right sidebar toggle -->
	        <!-- ============================================================== -->
	        <!-- ============================================================== -->
	        <!-- Container fluid  -->
	        <!-- ============================================================== -->
	        <div class="container-fluid">
	            <!-- ============================================================== -->
	            <!-- Applications  -->
	            <!-- ============================================================== -->
	          	<?= $this->fetch('content') ?>
	        </div>
	        <!-- ============================================================== -->
	        <!-- End Container fluid  -->
	        <!-- ============================================================== -->
	        <!-- ============================================================== -->
	        <!-- footer -->
	        <!-- ============================================================== -->
	        <footer class="footer text-center">
	            Communication Portal Copyright @ 2018
	        </footer>
	        <!-- ============================================================== -->
	        <!-- End footer -->
	        <!-- ============================================================== -->
	    </div>
	    <!-- ============================================================== -->
	    <!-- End Page wrapper  -->
	    <!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
	<?= $this->Html->script([
		'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
		'https://code.jquery.com/jquery-3.3.1.min.js',
		// Bootstrap tether Core JavaScript
		'libs/popper.js/dist/umd/popper.min',
		'libs/bootstrap/dist/js/bootstrap.min',
		'libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min',
		'extra-libs/sparkline/sparkline',
		'waves',
		'sidebarmenu',
		'custom.min',
		'extra-libs/multicheck/datatable-checkbox-init',
		'extra-libs/multicheck/jquery.multicheck',
		'extra-libs/DataTables/datatables.min',
		'libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
		'libs/quill/dist/quill.min',
		'libs/toastr/build/toastr.min',
		'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
		'dist/jquery.validate',
		'communication',
		'Connection',
		'communication/communication_list',
		'libs/select2/dist/js/select2.full.min',
		'libs/select2/dist/js/select2.min',
		'og_portal'
	]) ?>
    <!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal with Dynamic Content</h4>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script type="text/javascript">

		var BASE_URL = "<?= BASE_URL; ?>";
		var Broadcast = {
			POST : "<?php echo POST; ?>",
			BROADCAST_URL : "<?php echo BROADCAST_URL; ?>",
			BROADCAST_PORT : "<?php echo BROADCAST_PORT; ?>",
		};
		var connection = new Connection(Broadcast.BROADCAST_URL+":"+Broadcast.BROADCAST_PORT);
		console.log(connection);

		$(function () {
			$('#communication_table').DataTable({ "dom": '<"top"i>rt<"bottom"flp><"clear">' });
			$('#usersTable,#companyApplicationTable,#applicationsTable,#companiesTable,#userCommentsTable').DataTable({ "dom": '<"top"i>rt<"bottom"flp><"clear">' });
			$(".dataTables_length").attr('style','position: absolute;margin-left: 60%');
			$(".dataTables_length").children().attr('style','margin-right: 223px');
			$(".dataTables_filter").children().attr('style','position: absolute;top: 73px; left: 182px');
			$(".dataTables_info").attr('style','position: absolute; top: 90%; left: 45%;');
			$('.select2').select2({
				placeholder: "Please Select",
				allowClear: true
			});

			$('.select2').on('change', function() {
				var data = $(".select2 option:selected").val();
				console.log(data);
			})
			var issue = ("<?php echo $checkIssues ?>");

			if(issue > 0) 
			{
				$('.communicationProblem').trigger('click');
			}

			$('.communicationProblem').click(function(){
				$('#communicatioModal').modal({
					backdrop: 'static',
					keyboard: false
				});
			}); 
			
			$('#previous_communication').DataTable( {
				drawCallback: function() {
					$('[data-toggle="popover"]').popover();
				} 
			} );

			$('[data-toggle="tooltip"]').tooltip();
			$(".preloader").fadeOut();
		});
	</script>

</body>

</html>
