<div class="card">
	<div class="card-body">
		<h3  class="card-title"><b>User Likes Management</b></h3>
		<div class="p5">
		<?php
			echo $this->Html->link(
				'<i class="fa fa-undo"></i> Back',
			[
				'controller' => 'Communications',
				'action' => 'lists',
				$list_id
			],
			[ 
				'escape' => false,
				'class' => 'btn btn-warning'
			]
			)
		?>
		</div>
		<div class="table-responsive">
			<table id="userCommentsTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Username</th>
						<th>Like Date & Time</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($userlikes as $userlike): ?>
						<tr>
							<td><?= h($userlike->user['firstname']).' '.h($userlike->user['lastname']) ?></td>
							<td><?= h($userlike->created) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>