<div class="card">
	<div class="card-body">
		<h3 class="card-title"><b>Archived List</b></h3>
		<div class="table-responsive">
			<div class="p5 l10">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-undo"></i> List',
						[
							'controller' => 'Communications',
							'action' => 'lists',
							$id
						],
						[ 
							'escape' => false,
							'class' => 'btn btn-primary btn-sm'
						]
					)
				?>
			</div>
			<table id="communication_table" class="table table-bordered">
				<thead>
					<tr>
						<th>Title</th>
						<th>Author</th>
						<th>Release</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($communicationlist as $communication): ?>
					<tr style="<?= h($communication->status == 1)? 'font-weight: bold' : ''; ?>">
						<td><?= ucfirst(h($communication->author)) ?></td>
						<td><?= ucfirst(h($communication->heading)) ?></td>
						<td><?= h($communication->created) ?></td>
						<td class="actions">
							<?= $this->Html->link(__('<i class="fas fa-undo"></i>  Active'), ['action' => 'edit', $communication->id, $id], ['class' => 'btn btn-sm btn-info', 'escape' => false]) ?>
							<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Remove Completely'), ['action' => 'delete', $communication->id, $id], ['confirm' => __('Are you sure you want to remove this completely?', $communication->id), 'escape' => false, 'class' => 'btn btn-sm btn-danger']) ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><span id="view_title"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="loadingAnimated" id="view_content"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
