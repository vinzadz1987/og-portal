<div class="col-sm-12" id="communication_list">
	<div id="realtimeCommunication"></div>
	<div class="card">
		<div class="card-body">
			<h3 class="card-title"><b>Communications List</b></h3>
			<div class="p5 l10">
				<?php if($globalAuth['company_role'] == 0): ?>
					<button class="btn btn-sm btn-primary addBtn"><i class="fas fa-plus"></i>  Add Communication</button>
				<?php else: ?> 
					<div style="margin-bottom: 30px;">
				<?php endif;?>
			</div>
			<div class="table-responsive">
				<div id="realtimeCommunication"></div>
				<table id="communication_table" class="table table-bordered">
					<thead>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Released</th>
							<th>Acknowledge</th>
							<?php if($globalAuth['company_role'] == 0): ?>
								<th>Actions</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($communicationlist as $communication): ?>
						<tr style="<?= h($communication->status == 1)? 'font-weight: bold' : ''; ?>">
							<td>
								<a href="" class="commTitle" data-id="<?= $communication->id ?>" data-toggle="modal" data-target="#modal<?= $communication->id ?>" 
									onclick="showComment(this, 1)"><?= ucfirst(h($communication->heading)) ?></a>
								<div class="modal fade" id="modal<?= $communication->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">
													<h4><?php echo ucfirst(h($communication->heading)); ?></h4>
												</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<?= h($communication->message) ?><br>
												<span class="text-muted float-right"><small>Release: <?= h($communication->created) ?></small></span>
											</div>
											<div class="modal-footer">
												<button class='btn btn-info btn-xs commentBtn<?= h($communication->id) ?>' data-id="<?= h($communication->id) ?>" onclick="showComment(this, 2)" data-toggle="popover"><i class='m-r-10 mdi mdi-comment'></i>Comment</button> 
												<span class="label label-success float-right coun countCommentUser<?= h($communication->id) ?>" data-id="<?= h($communication->id) ?>" style="cursor: pointer;" onclick="showUserComment(this)">0</span>
												<button class='btn btn-cyan btn-xs likeComment' data-user="<?=$sess_user_id?>" data-id="<?= h($communication->id) ?>"><i class='m-r-10 mdi mdi-thumb-up'></i> Like</button> <span class="label label-info float-right likeCount<?= h($communication->id) ?>"  data-id="<?= h($communication->id) ?>" style="cursor: pointer;" onclick="showUserLike(this)">0</span>
												<?php if($globalAuth['company_role'] == 0): ?>
													<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Archive'), ['action' => 'toarchived', $communication->id, $id], ['confirm' => __('Are you sure you want to archive this communication?'), 'class' => 'btn btn-danger btn-xs', 'style' => "margin-left: 80px", 'escape' => false]) ?>
												<?php endif; ?> 
											</div>
											<div class="commentBox p5disno" id="commentBox<?= $communication->id ?>">
												<div class="commentContent"></div>
												<input type="hidden" class="userid" name="userid" value="<?=$sess_user_id?>">
												<input type="hidden" class="username" name="username" value="<?=$sess_username?>">
												<textarea class="form-control commentText" placeholder="Please enter you comment" style="height: 150px;"></textarea>
												<button class='btn btn-info btn-xs float-right postComment' data-id="<?= $communication->id ?>" style="margin-top: 5px;">Post</button>
											</div>
											<div id="commentHere<?= $communication->id ?>" class="disno"></div>
											<div id="view_content<?= $communication->id ?>"></div>
										</div>
									</div>
								</div>
								</td>
							<td><?= ucfirst(h($communication->author)) ?></td>
							<td><?php $date = new DateTime(h($communication->created)); echo $date->format('Y/m/d H:i a'); ?></td>
							<td>
								<?php
									$cnt_ack = explode(',', h($communication->acknowledge));
									$countack = (!empty(h($communication->acknowledge)))? count($cnt_ack) : 0;
									echo $this->Html->link(
											'<i class="fa fa-users"></i> <b>'.$countack.'</b>',
										[
											'controller' => 'Communications',
											'action' => 'acknowledge',
											h($communication->id),
											$id

										],
										[ 
											'escape' => false
										]
									)
								?>
							</td>
							<?php if($globalAuth['company_role'] == 0): ?>
								<td class="actions">
									<div class="btn-group">
										<?php $ident = ''; if(h($communication->status) == 1): ?>
											<?php $ident = 'btn-primary'; ?>
										<?php elseif (h($communication->status == 2)): ?>
											<?php $ident = 'btn-success'; ?>
										<?php else: ?>
											<?php $ident = 'btn-danger'; ?>
										<?php endif; ?>
										<button type="button" class="btn <?= $ident ?> dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<?php if(h($communication->status) == 1): ?>
												<i class="fa fa-edit"></i> Draft
											<?php elseif (h($communication->status == 2)): ?>
												<i class="fa fa-check"></i> Active
											<?php else: ?>
												<i class="fa fa-trash"></i> Archived
											<?php endif; ?>
										</button>
										<div class="dropdown-menu">
											<?php
												echo $this->Html->link(
														'<i class="fa fa-edit"></i> Draft',
													[
														'controller' => 'Communications',
														'action' => 'edit',
														$communication->id, 
														$id
													],
													[ 
														'escape' => false,
														'class' => 'dropdown-item'
													]
												)
											?>
											<?= $this->Form->postLink(__('<i class="fa fa-check"></i> Active'), ['controller' => 'Communications','action' => 'updatestatus', $communication->id, 2, $id], ['class' => 'dropdown-item', 'escape' => false]) ?>
											<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Archive'), ['controller' => 'Communications','action' => 'updatestatus', $communication->id, 3, $id], ['class' => 'dropdown-item', 'escape' => false]) ?>
										</div>
									</div>				
								</td>
							<?php endif; ?>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="addCommDiv form col-md-4 large-9 medium-8 columns content float-right hidden bwhtp20">
	<?= $this->Form->create(null,['url' => ['controller' => 'Communications','action' => 'lists', $id],'id' => 'frmAddCommu']) ?>
		<fieldset>
			<h3 class="card-title"><b>Add Communication</b></h3>
			<?= $this->Form->control('author', ['class' => 'form-control', 'required' => true]) ?>
			<?= $this->Form->control('start_date', ['class' => 'form-control', 'type' => 'text', 'value' => date("Y-m-d"), 'required' => true]) ?>
			<?= $this->Form->control('stop_date', ['class' => 'form-control', 'type' => 'text', 'required' => true]) ?>
			<?=  $this->Form->control('heading', ['class' => 'form-control', 'required' => true]) ?>
			<?= $this->Form->control('message', ['type' => 'textarea','class' => 'form-control', 'required' => true]) ?>
			<?= $this->Form->control('application', ['type' => 'hidden', 'value' => $id]) ?>
			<?= $this->Form->control('company', ['type' => 'hidden', 'value' => $globalAuth['company']]) ?>
			<div style="display: none;">
				<?=  $this->Form->control('status', ['type' => 'text']) ?>
			</div>
		</fieldset>
		<hr>
		<?= $this->Form->button(__('<i class="fas fa-check"></i> Save & Exit'), ['class' => 'btn btn-primary btn-md','name' => 'save', 'escape' => false]) ?>
		<span class="btn btn-danger btn-md cancelAdd"><i class="fas fa-times"></i> Cancel</span>
		<?= $this->Form->button(__('<i class="fas fa-check-square"></i> Publish'), ['class' => 'btn btn-primary btn-md', 'name' => 'publish', 'escape' => false]) ?>
	<?= $this->Form->end() ?>
</div>
