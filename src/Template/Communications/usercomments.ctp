<div class="card">
	<div class="card-body">
		<h3  class="card-title"><b>Acknowledge</b></h3>
		<div class="p5">
		<?php
			echo $this->Html->link(
				'<i class="fa fa-undo"></i> Back',
			[
				'controller' => 'Communications',
				'action' => 'lists',
				$list_id
			],
			[ 
				'escape' => false,
				'class' => 'btn btn-warning'
			]
			)
		?>
		</div>
		<div class="table-responsive">
			<table id="userCommentsTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Username</th>
						<th>Comment</th>
						<th>Commented Date & Time</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usercomments as $usercomment): ?>
						<tr>
							<td><?= h($usercomment->user['firstname']).' '.h($usercomment->user['lastname']) ?></td>
							<td><?= h($usercomment->comment) ?></td>
							<td><?= h($usercomment->created) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>