<div class="card">
	<div class="card-body">
		<h3  class="card-title"><b>User Acknowledge</b></h3>
		<div class="p5">
		<?php
			echo $this->Html->link(
				'<i class="fa fa-undo"></i> Back',
			[
				'controller' => 'Communications',
				'action' => 'lists',
				$app_id
			],
			[ 
				'escape' => false,
				'class' => 'btn btn-warning'
			]
			)
		?>
		</div>
		<div class="table-responsive">
			<table id="userCommentsTable" class="table table-bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>Acknowledge Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($acknowledges as $acknowledge): ?>
						<tr>
							<td><?= h($acknowledge->user['firstname']).' '.h($acknowledge->user['lastname']) ?></td>
							<td><?= h($acknowledge->created) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>