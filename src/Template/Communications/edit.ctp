<div class="card">
	<?= $this->Form->create($communication, ['class' => 'form-horizontal']) ?>
	<div class="card-body">
		<h4 class="card-title">Edit Communication</h4>
		<?= $this->Form->control('author', ['placeholder' => 'Complete Name', 'class' => 'form-control', 'label' => 'Author']) ?>
		<?= $this->Form->control('start_date', ['class' => 'form-control', 'type' => 'text', 'value' => date('Y-m-d'), 'label' => 'Start Date']) ?>
		<?= $this->Form->control('stop_date', ['class' => 'form-control', 'type' => 'text', 'label' => 'Stop Date']) ?>
		<?=  $this->Form->control('heading', ['class' => 'form-control', 'label' => 'Title']) ?>
		<?= $this->Form->control('message', ['type' => 'textarea','class' => 'form-control']) ?>
		<div style="display: none;">
			<?=  $this->Form->control('status', ['type' => 'text']) ?>
		</div>
	</div>
	<div class="border-top">
		<div class="card-body">
			<?= $this->Form->button(__('<i class="fa fa-save"></i> Save & Exit'), ['class' => 'btn btn-primary','name' => 'save', 'escape' => false]) ?>
			<?php
				echo $this->Html->link(
					'<i class="fa fa-times"></i> Cancel',
				[
					'controller' => 'Communications',
					'action' => 'lists',
					$app_id
				],
				[ 
					'escape' => false,
					'class' => 'btn btn-danger btn-md'
				]
				)
			?>
			<?= $this->Form->button(__('<i class="fa fa-check"></i> Publish'), ['class' => 'btn btn-primary', 'name' => 'publish', 'escape' => false]) ?>
		</div>
	</div>
	<?php
	$link = $_SERVER['REQUEST_URI'];
	$link_array = explode('/',$link);
	$lastUri = end($link_array);
	?>
	<?= $this->Form->end() ?>
</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min']) ?>
<script type="text/javascript">
	$(function () {
		$("button[name='save']").on('click', function() {
			$("#status").val('1');
		});
		$("button[name='publish']").on('click', function() {
			$("#status").val('2');
		});
		$("form").on("submit", function( event) {
			event.preventDefault();
			$.ajax({
				type: 'post',
				async: true,
				cache: false,
				url:'<?= $lastUri ?>',
				success: function (response) {
					var pj = $.parseJSON(response);
					if(pj.result == "success") {
						toastr.success('Updated communication succesfully.','Success');
						$("#status").val('');
						window.setTimeout( function () {
							window.location.href = '<?=BASE_URL?>communications/lists/<?= $app_id ?>';
						}, 3000);
					} else {
						toastr.error('Something error in updating communication.', 'Error!');
					}
				},
				error: function(response) {
					toastr.error('Something error in updating communication.', 'Error!');
				},
				data: $("form").serialize()
			});
		});
	});
</script>

