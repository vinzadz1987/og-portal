<div class="users form col-md-4 large-9 medium-8 columns content bwhtp20">
	<?= $this->Form->create($user) ?>
		<fieldset>
			<legend class="text-center"><?= __('Update User') ?></legend>
			<?php
				echo $this->Form->control('firstname',['class' => 'form-control']);
				echo $this->Form->control('lastname',['class' => 'form-control']);
			?>

			<?php
				echo $this->Form->control('email',['class' => 'form-control']);
				echo $this->Form->control('password',['class' => 'form-control']);
			?>
			<?php 
				$hide = ''; $companyauth = '';
				if($userRole == "User") {
					$hide = 'hide';
					$companyauth = $globalAuth['company'];
				}
			?>
			<div class="<?=$hide?>">
				<?php 
					echo $this->Form->control('company', [
						'options' => $company,
						'empty' => 'Please Select',
						'class' => ['form-control'],
						'value' => $companyauth,
						'required' => true,
					]);
				?>
			</div>
			<?php 
				echo $this->Form->control('company_role', [
					'options' => ['Admin', 'Staff'],
					'empty' => 'Please Select',
					'class' => ['form-control'],
					'required' => true
				]); 
			?>
			<?php echo $this->Form->control('role', ['type' => 'hidden', 'value' => 'User']); ?>
		</fieldset>
		<hr>
		<?= $this->Form->button(__('<i class="fas fa-save"></i> Save'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
		<?= $this->Html->link(__('<i class="fas fa-undo"></i> Cancel'), ['action' => 'index'], ['class' => 'btn btn-warning', 'escape' => false]) ?>
	<?= $this->Form->end() ?>
</div>


