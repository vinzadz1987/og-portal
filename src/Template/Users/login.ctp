<div>
	<div id="loginform" style="background: rgba(0, 0, 0, 0.6); padding: 100px;">
	    <div class="text-center p-t-20 p-b-20">
	        <span class="db">
	        	<h2 class="little-big-header">Outsourced Global</h2>
	        	<h5 class="small-header">PORTAL</h5>
	        </span>
	    </div>
		<?= $this->Form->create(null, ['class' => 'form-horizontal m-t-20']) ?>
	        <div class="row p-b-30">
	            <div class="col-12">
	                <div class="input-group mb-3">
	                    <div class="input-group-prepend">
	                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
	                    </div>
	                    <?= $this->Form->control('email', ['class' => 'form-control form-control-lg', 'placeholder' => 'test@example.com', 'label' => false, 'style' => 'width: 320px', 'required' => true]) ?>
	                </div>
	                <div class="input-group mb-3">
	                    <div class="input-group-prepend">
	                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
	                    </div>
	                    <?= $this->Form->control('password', ['class'=>'form-control form-control-lg', 'placeholder' => 'Password', 'label' => false, 'style' => 'width: 320px', 'required' => true]) ?>
	                </div>
	            </div>
	        </div>
	        <div class="row border-top border-secondary">
	            <div class="col-12">
	                <div class="form-group">
	                    <div class="p-t-20">
	                        <button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button>
	                        <?= $this->Form->button('<i class="fas fa-sign-in-alt"></i> Login', ['class' => 'btn btn-success float-right', 'escape' => false]) ?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    <?= $this->Form->end() ?>
	</div>
	<div id="recoverform">
	    <div class="text-center">
	        <span class="text-white">Enter your e-mail address below and we will send you instructions how to recover a password.</span>
	    </div>
	    <div class="row m-t-20">
	        <!-- Form -->
	        <form class="col-12" action="index.html">
	            <!-- email -->
	            <div class="input-group mb-3">
	                <div class="input-group-prepend">
	                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
	                </div>
	                <input type="text" class="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
	            </div>
	            <!-- pwd -->
	            <div class="row m-t-20 p-t-20 border-top border-secondary">
	                <div class="col-12">
	                    <a class="btn btn-success" href="#" id="to-login" name="action">Back To Login</a>
	                    <button class="btn btn-info float-right" type="button" name="action">Recover</button>
	                </div>
	            </div>
	        </form>
	    </div>
	</div>
<?= $this->Html->script(['libs/jquery/dist/jquery.min']) ?>
</div>
 <script>
// ============================================================== 
// Login and Recover Password 
// ============================================================== 
$('#to-recover').on("click", function() {
	$("#loginform").slideUp();
	$("#recoverform").fadeIn();
});
$('#to-login').click(function(){

	$("#recoverform").hide();
	$("#loginform").fadeIn();
});
</script>