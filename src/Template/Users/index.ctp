<div class="col-sm-12" id="usersList">
	<div class="card">
		<div class="card-body">
			<h3 class="card-title"><b>Users</b></h3>
			<div class="p5">
				<!-- <?= $this->Html->link(__('<i class="fas fa-plus"></i> Add User'), ['action' => 'add'], ['class' => 'btn btn-primary', 'escape' => false]) ?> -->
				<button class="btn btn-sm btn-primary addUserBtn"><i class="fas fa-plus"></i>  Add User</button>
			</div>
			<div class="table-responsive">
				<table id="usersTable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Name</th>
							<?php 
								if($userRole != "User") {
									echo "<th>Company</th>";
								}
							?>
							<th>Role</th>
							<th>Created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user): ?>
							<tr>
								<td><?= h($user->firstname).' '.h($user->lastname) ?></td>
								<?php if($userRole != "User"): ?>
								<td><?php if(h($user->company) !== 0){ echo $company[h($user->company)]; } ?></td>
								<?php endif; ?>
								<td>
								<?php
									$company_role = ['Admin', 'Staff'];
									echo $company_role[h($user->company_role)]; 
								?>
								</td>
								<td><?= h($user->created) ?></td>
								<td class="actions">
								<?= $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), ['action' => 'edit', $user->id], ['class' => 'btn btn-primary btn-xs', 'escape' => false]) ?>
								<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Archive'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id), 'class' => 'btn btn-danger btn-xs', 'escape' => false]) ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="users form col-md-4 large-9 medium-8 columns content bwhtp20 addUserDiv hidden">
	<?= $this->Form->create(null,['url' => ['controller' => 'Users','action' => 'add'],'id' => 'frmAddUser']) ?>
		<fieldset>
			<h3 class="card-title"><b><?= __('Add User') ?></b></h3>
			<?php
				echo $this->Form->control('firstname',['class' => 'form-control']);
				echo $this->Form->control('lastname',['class' => 'form-control']);
			?>

			<?php
				echo $this->Form->control('email',['class' => 'form-control']);
				echo $this->Form->control('password',['class' => 'form-control']);
			?>
			<?php 
				$hide = ''; $companyauth = '';
				if($userRole == "User") {
					$hide = 'hide';
					$companyauth = $globalAuth['company'];
				}
			?>
			<div class="<?=$hide?>">
				<?php 
					echo $this->Form->control('company', [
						'options' => $company,
						'empty' => 'Please Select',
						'class' => ['form-control'],
						'value' => $companyauth,
						'required' => true,
					]);
				?>
			</div>
			<?php 
				echo $this->Form->control('company_role', [
					'options' => ['Admin', 'Staff'],
					'empty' => 'Please Select',
					'class' => ['form-control'],
					'required' => true
				]); 
			?>
			<?php echo $this->Form->control('role', ['type' => 'hidden', 'value' => 'User']); ?>
		</fieldset>
		<hr>
	<?= $this->Form->button(__('<i class="fa fa-save"></i> Save'), ['class' => 'btn btn-primary', 'escape' => false]) ?>
	<span class="btn btn-danger btn-md cancelAddUser"><i class="fas fa-times"></i> Cancel</span>
	<?= $this->Form->end() ?>
</div>
