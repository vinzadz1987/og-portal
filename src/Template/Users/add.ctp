<div class="users form col-md-4 large-9 medium-8 columns content bwhtp20">
	<?= $this->Form->create($user) ?>
	<fieldset>
		<legend class="text-center"><?= __('Add User') ?></legend>
		<?php
			echo $this->Form->control('firstname',['class' => 'form-control']);
			echo $this->Form->control('lastname',['class' => 'form-control']);
		?>

		<?php
			echo $this->Form->control('email',['class' => 'form-control']);
			echo $this->Form->control('password',['class' => 'form-control']);
		?>
		<?php 
			$hide = '';
			if($role == "User") {
				$hide = 'hide';
			}
		?>
		<div class="<?= $hide ?>">
			<?php 
				echo $this->Form->control('company', [
					'options' => $company,
					'empty' => 'Please Select',
					'class' => ['form-control']
				]);
			?>
		</div>
		<?php 
			echo $this->Form->control('company_role', [
				'options' => ['Admin', 'Staff'],
				'empty' => 'Please Select',
				'class' => ['select2 form-control']
			]); 
		?>
		<?php echo $this->Form->control('role', ['type' => 'hidden', 'value' => 'User']); ?>
	</fieldset>
	<hr>
	<?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary']) ?>
	<?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-warning']) ?>
	<?= $this->Form->end() ?>
</div>


