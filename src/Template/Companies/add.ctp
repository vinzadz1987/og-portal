<div class="card">
	<div class="card-body table-responsive">
		<div class="companies form large-9 medium-8 columns content">
			<?= $this->Form->create($company) ?>
			<fieldset>
				<legend><?= __('Add Company') ?></legend>
				<?php
					echo $this->Form->control('name',['class' => 'form-control']);
					echo $this->Form->control('description',['class' => 'form-control']);
				?>
			</fieldset>
			<hr>
			<?= $this->Form->button(__('Save'),['class' => 'btn btn-primary']) ?>
			<?= $this->Html->link(__('Cancel'), ['action' => 'index'],['class' => 'btn btn-danger']) ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>
