<div class="card">
	<div class="card-body">
		<h3 class="card-title"><b><?= $company ?> Company <?php if ( $company_role == 1 ): echo 'Staff';  else:  echo 'Admin'; endif; ?></b></h3>
		<div style="padding: 5px">
			<?php
				echo $this->Html->link(
					'<i class="fa fa-undo"></i> Back',
					[
						'controller' => 'Companies',
						'action' => 'index'
					],
					[ 
						'escape' => false,
						'class' => 'btn btn-warning btn-sm'
					]
				)
			?>
		</div>
		<div class="table-responsive">
			<table id="usersTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Created</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($admins as $admin): ?>
					<tr>
						<td><?= h($admin->firstname) ?></td>
						<td>
							<?= h($admin->lastname) ?>
						</td>
						<td><?= h($admin->created) ?></td>
						<td class="actions">
							<div class="btn-group">
								<button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-edit"></i></button>
								<div class="dropdown-menu">
									<?php if ( $company_role == 1 ): ?> 
										<?= $this->Form->postLink(__('To Admin'), ['controller' => 'companies','action' => 'addmin', $admin->id, $company_role], ['class' => 'dropdown-item']) ?>
									<?php else: ?>
										<?= $this->Form->postLink(__('To Staff'), ['controller' => 'companies','action' => 'addmin', $admin->id, $company_role], ['class' => 'dropdown-item']) ?>
									<?php endif; ?>
								</div>
							</div>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>