<div class="card">
	<div class="card-body">
		<h3 class="card-title"><b>Company Admin</b></h3>
		<div style="padding: 5px">
			<?php
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Add Admin',
					[
						'controller' => 'Companies',
						'action' => 'add'
					],
					[ 
						'escape' => false,
						'class' => 'btn btn-primary btn-sm'
					]
				)
			?>
			<?php
				echo $this->Html->link(
					'<i class="fa fa-undo"></i> Back',
					[
						'controller' => 'Companies',
						'action' => 'index'
					],
					[ 
						'escape' => false,
						'class' => 'btn btn-warning btn-sm'
					]
				)
			?>
		</div>
		<div class="table-responsive">
			<table id="usersTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Created</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($admins as $admin): ?>
					<tr>
						<td><?= h($admin->firstname) ?></td>
						<td>
							<?= h($admin->lastname) ?>
						</td>
						<td><?= h($admin->created) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>