<div class="card">
	<div class="card-body">
		<h3 class="card-title"><b>Company</b></h3>
		<div style="padding: 5px">
			<?php
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Add Company',
					[
						'controller' => 'Companies',
						'action' => 'add'
					],
					[ 
						'escape' => false,
						'class' => 'btn btn-primary btn-sm'
					]
				)
			?>
		</div>
		<div class="table-responsive">
			<table id="companiesTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Company Name</th>
						<th>Administrator</th>
						<th>Created</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($companies as $company): ?>
					<tr>
						<td><?= h($company->name) ?></td>
						<td>
							<?php
								echo $this->Html->link(
										'<i class="fa fa-users"></i> Admin',
									[
										'controller' => 'Companies',
										'action' => 'admins',
										h($company->id),
										0,
										h($company->name)
									],
									[ 
										'escape' => false,
										'class' => 'btn btn-primary btn-sm'
									]
								)
							?>
							<?php
								echo $this->Html->link(
										'<i class="fa fa-users"></i> Staff',
									[
										'controller' => 'Companies',
										'action' => 'admins',
										h($company->id),
										1,
										h($company->name)
									],
									[ 
										'escape' => false,
										'class' => 'btn btn-success btn-sm'
									]
								)
							?>
						</td>
						<td><?= h($company->created) ?></td>
						<td class="actions">
							<?= $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), ['action' => 'edit', $company->id], ['class' => 'btn btn-primary btn-xs', 'escape' => false]) ?>
							<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $company->id], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id), 'class' => 'btn btn-danger btn-xs', 'escape' => false]) ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>