<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav" class="p-t-30">
				<?php if($userRole == "Admin"): ?>
					<?php foreach ( $globalMenuAdmin as $menu ): ?>
						<li class="sidebar-item">
							<?php
								$has_sub = '';
								if(h($menu->sub) !== "") { $has_sub = 'has-arrow'; } ;
								echo $this->Html->link(
									'<i class="'.h($menu->icon).'"></i> <span class="hide-menu">'.h($menu->name).'</span>',
									[
										'controller' => h($menu->controller),
										'action' => h($menu->action)
									],
									[ 
										'escape' => false, 
										'class' => 'sidebar-link waves-effect waves-dark sidebar-link '.$has_sub.''
									]
								)
							?>
							<?php if(h($menu->sub) !== "") { ?>
								<ul aria-expanded="false" class="collapse  first-level">
									<?php 
										$sub_menu = explode(',',h($menu->sub)); 
									?>
									<?php for($i=0; $i < sizeof($sub_menu); $i++) {?>
										<li class="sidebar-item">
											<?php
												echo $this->Html->link(
													'<i class="mdi mdi-plus-box-outline"></i> <span class="hide-menu">'.$submenus[$sub_menu[$i]]->name.'</span>',
													[
														'controller' => $submenus[$sub_menu[$i]]->controller,
														'action' => $submenus[$sub_menu[$i]]->action
													],
													[ 
														'escape' => false,
														'class' => 'sidebar-link' 
													]
												)
											?>
										</li>
									<?php } ?>
								</ul>
							<?php } ?>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php if($userRole == "User"): ?>

					<?php foreach ( $menus_user as $menu ): ?>
						<li class="sidebar-item">
							<?php
								echo $this->Html->link(
									'<i class="'.h($menu->icon).'"></i> <span class="hide-menu">'.h($menu->name).'</span>',
									[
										'controller' => h($menu->controller),
										'action' => h($menu->action)
									],
									[ 
										'escape' => false, 
										'class' => 'sidebar-link waves-effect waves-dark sidebar-link'
									]
								)
							?>
						</li>
					<?php endforeach; ?>
					<?php if($globalAuth['company_role'] == "Staff"): ?>
						<li class="sidebar-item">
							<?php
								echo $this->Html->link(
									'<i class="fas fa-users"></i> Users',
									[
										'controller' => 'Users',
										'action' => 'index'
									],
									[ 
										'escape' => false, 
										'class' => 'sidebar-link waves-effect waves-dark sidebar-link'
									]
								)
							?>
						</li>
					<?php endif; ?>
					<?php foreach ( $globalUserMenu as $key => $menu ): ?>
						<li class="sidebar-item">
							<?php
								echo $this->Html->link(
									'<img src="'.$this->request->getAttribute("webroot") .$menu->icon.'" class="rounded-circle" style="height: 25px" /> <span class="hide-menu" style="margin-left: 5px">'.ucfirst(h($menu->name)).'</span>',
									[
										'controller' => 'Communications',
										'action' => 'lists',
										$menu->id
									],
									[ 
										'escape' => false, 
										'class' => 'sidebar-link waves-effect waves-dark sidebar-link has-arrow'
									]
								)
							?>
							<ul aria-expanded="false" class="collapse  first-level">
									<li class="sidebar-item">
										<?php
											echo $this->Html->link(
												'<i class="mdi mdi-format-line-weight"></i> <span class="hide-menu">List</span>',
												[
													'controller' => 'Communications',
													'action' => 'lists',
													$menu->id
												],
												[ 
													'escape' => false,
													'class' => 'sidebar-link' 
												]
											)
										?>
									</li>
									<li class="sidebar-item">
										<?php
											echo $this->Html->link(
												'<i class="mdi mdi-archive"></i> <span class="hide-menu">Archived</span>',
												[
													'controller' => 'Communications',
													'action' => 'archived',
													$menu->id
												],
												[ 
													'escape' => false,
													'class' => 'sidebar-link' 
												]
											)
										?>
									</li>
							</ul>
						</li>
					<?php endforeach; ?>
				<?php endif; ?>
			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->