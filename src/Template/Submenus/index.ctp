<div class="card">
    <div class="card-body">
        <div class="submenus index large-9 medium-8 columns content">
            <h3><?= __('Submenus') ?></h3>
            <div style='padding:10px'>
                <?= $this->Html->link(__('New Submenu'), ['action' => 'add'], ['class' => 'btn btn-primary']) ?>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('controller') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('action') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($submenulist as $submenu): ?>
                    <tr>
                        <td><?= $this->Number->format($submenu->id) ?></td>
                        <td><?= h($submenu->name) ?></td>
                        <td><?= h($submenu->controller) ?></td>
                        <td><?= h($submenu->action) ?></td>
                        <td><?= h($submenu->created) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $submenu->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $submenu->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $submenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $submenu->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
