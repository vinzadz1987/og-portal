<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submenu $submenu
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $submenu->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $submenu->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Submenus'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="submenus form large-9 medium-8 columns content">
    <?= $this->Form->create($submenu) ?>
    <fieldset>
        <legend><?= __('Edit Submenu') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('controller');
            echo $this->Form->control('action');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
