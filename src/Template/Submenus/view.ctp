<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Submenu $submenu
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Submenu'), ['action' => 'edit', $submenu->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Submenu'), ['action' => 'delete', $submenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $submenu->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Submenus'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Submenu'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="submenus view large-9 medium-8 columns content">
    <h3><?= h($submenu->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($submenu->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Controller') ?></th>
            <td><?= h($submenu->controller) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Action') ?></th>
            <td><?= h($submenu->action) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($submenu->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($submenu->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($submenu->description)); ?>
    </div>
</div>
