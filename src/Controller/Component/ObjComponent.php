<?php 

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\QueryInterface;
use Cake\Datasource\RepositoryInterface;
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;

class ObjComponent extends Component
{
	// selection
	public function selection($table) {
		$this->Table = TableRegistry::get($table);
		$makeselect = $this->Table->find('list',['keyField' => 'id', 'valueField' => 'name']);
		return $makeselect->toArray();
	}

	// selection
	public function selection_VF1($table) {
		$this->Table = TableRegistry::get($table);
		$makeselect = $this->Table->find('list',[
			'keyField' => 'id',
			'valueField' => function ($row) {
				return $row['name'] . ' - ' . $row['description'];
			}
		]);
		return $makeselect->toArray();
	}

	// find to array
	public function fta($table) {
		$this->Table = TableRegistry::get($table);
		$makeselect = $this->Table->find();
		return $makeselect->toArray();
	}

	// find to array with where
	public function ftaWhere($table,$where,$value) {
		$this->Table = TableRegistry::get($table);
		$makeselect = $this->Table->find('all')->where([$where => $value]);
		return $makeselect->toArray();
	}
}