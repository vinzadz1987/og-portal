<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Submenus Controller
 *
 * @property \App\Model\Table\SubmenusTable $Submenus
 *
 * @method \App\Model\Entity\Submenu[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubmenusController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'index',
            'view',
            'add',
            'edit',
            'delete'
        ]);

        $this->viewBuilder()->setLayout('home');
    }

    public function index()
    {
        $submenulist = $this->paginate($this->Submenus);

        $this->set(compact('submenulist'));
    }


    public function view($id = null)
    {
        $submenu = $this->Submenus->get($id, [
            'contain' => []
        ]);

        $this->set('submenu', $submenu);
    }

    public function add()
    {
        $submenu = $this->Submenus->newEntity();
        if ($this->request->is('post')) {
            $submenu = $this->Submenus->patchEntity($submenu, $this->request->getData());
            if ($this->Submenus->save($submenu)) {
                $this->Flash->success(__('The submenu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submenu could not be saved. Please, try again.'));
        }
        $this->set(compact('submenu'));
    }

    public function edit($id = null)
    {
        $submenu = $this->Submenus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $submenu = $this->Submenus->patchEntity($submenu, $this->request->getData());
            if ($this->Submenus->save($submenu)) {
                $this->Flash->success(__('The submenu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The submenu could not be saved. Please, try again.'));
        }
        $this->set(compact('submenu'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $submenu = $this->Submenus->get($id);
        if ($this->Submenus->delete($submenu)) {
            $this->Flash->success(__('The submenu has been deleted.'));
        } else {
            $this->Flash->error(__('The submenu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
