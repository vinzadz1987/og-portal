<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class CompaniesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([
            'index',
            'view',
            'add',
            'edit',
            'delete',
            'admins',
            'addmin'
        ]);

        $this->viewBuilder()->setLayout('home');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow([
            'addmin'
        ]);
        $actions = [
            'addmin'
        ];

        if (in_array($this->request->getParam('action'), $actions)) {
            $this->eventManager()->off($this->Csrf);
            $this->Security->config('unlockedActions', $actions);
        }
    }
    
    public function index()
    {
        $companies = $this->Companies->find();
        $this->set('companies',$companies);
    }

    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => []
        ]);

        $this->set('company', $company);
    }

	public function admins($id = null, $company_role = null, $company = null) {
		$admins = $this->Users->find()->where(['company' => $id, 'company_role' => $company_role]);
		$this->set(compact('admins','company','company_role'));
	}

	public function add()
	{
		$company = $this->Companies->newEntity();
		if ($this->request->is('post')) {
			$company = $this->Companies->patchEntity($company, $this->request->getData());
			if ($this->Companies->save($company)) {
				$this->Flash->success(__('The company has been saved.'));

				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('The company could not be saved. Please, try again.'));
		}
		$this->set(compact('company'));
	}

	public function addmin($id = null, $company_role = null)
	{
		$usersTable = TableRegistry::get('Users');
		$user = $usersTable->get($id);
		if($company_role == 1) {
			$company_role = 0;
		} else {
			$company_role = 1;
		}
		$user->company_role = $company_role;
		if($usersTable->save($user)) {
			$this->Flash->success(__('Updated Company Access.'));
		 	return $this->redirect(['action' => 'index']);
		}
	}

    public function edit($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('The company has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The company could not be saved. Please, try again.'));
        }
        $this->set(compact('company'));
    }
    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
