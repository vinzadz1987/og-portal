<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class AppController extends Controller
{

    public function initialize()
    {
		parent::initialize();
		$this->loadModel('Communications');
		$this->loadModel('Comments');
		$this->loadModel('Likes');
		$this->loadModel('Users');
		$this->loadModel('Menus');
		$this->loadModel('Submenus');
		$this->loadModel('Access');
        $this->loadModel('Roles');
        $this->loadModel('Companies');
        $this->loadModel('Acknowledges');

		$this->loadComponent('Security');
		$this->loadComponent('Obj');
		$this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller', 
        	'authenticate' => [
        		'Form' => [
        			'fields' => [
        				'username' => 'email',
        				'password' => 'password'
        			]
        		]
        	],
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
			],
			'loginRedirect' => [
				'controller' => 'Applications',
				'action' => 'dashboard'
			],
        	'unauthorizedRedirect' => [
                'controller' => 'Applications',
                'action' => 'company'
            ],
        ]);

		$users = $this->Obj->fta('Users');
		$globalMenuAdmin = $this->Obj->ftaWhere('Menus','category',1);
		$globalUserMenu = $this->Obj->ftaWhere('Applications','company LIKE','%'.$this->Auth->user('company').'%');
		$menus_user = $this->Menus->find()->where(['category' => 2]);
		$submenus = $this->Obj->fta('Submenus');
		$select_submenus = $this->Obj->selection_VF1('Submenus');
		$sess_username = $this->Auth->user('firstname').' '.$this->Auth->user('lastname');
		$sess_user_id = $this->Auth->user('id');
		$userRole = $this->Auth->user('role');
		$globalAuth = $this->Auth->user();
		$company_name =  $this->Companies->find()->where(['id' => $this->Auth->user('company')])->toArray();
		$gloBalCompName = isset($company_name[0]['name'])? $company_name[0]['name']: 'OG Portal';
		$gloBalApp = $this->Obj->selection('Applications');

		// problem checker
		if($this->Auth->user('role') == "User" && $this->Auth->user('company_role') == 1) {
			$checkIssues = $this->Communications->find()->where(['status' => 2, 'acknowledge not LIKE' => '%'.$this->Auth->user('id').'%'])->count();
		} else {
			$checkIssues = 0;
		}
		$checkIssues = isset($checkIssues)? $checkIssues : 0;
		
		$communication_type = ['General Communication', 'Critical Communication'];

		$this->set(compact(
			'communication_type',
			'checkIssues',
			'globalMenuAdmin',
			'menus_user',
			'submenus',
			'select_submenus',
			'access',
			'company',
			'sess_username',
			'sess_user_id',
			'userRole',
			'users',
			'gloBalCompName',
			'globalUserMenu',
			'globalAuth',
			'gloBalApp'
		));
    }
    
	public function isAuthorized($user)
	{
		return false;
	}
}
