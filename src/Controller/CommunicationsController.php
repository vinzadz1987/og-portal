<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class CommunicationsController extends AppController
{

	public function initialize(){
		parent::initialize();
        $this->Auth->allow([
            'index',
            'lists',
            'view',
            'add',
            'edit',
            'delete',
            'archived',
            'postcomment',
            'getcomment',
            'toarchived',
            'usercomments',
            'userlikes',
            'likecomment',
            'updatestatus',
            'acknowledge',
        ]);
		$this->viewBuilder()->setLayout('home');
		$this->loadComponent('RequestHandler');
	}

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow([
            'postcomment',
            'getcomment',
            'likecomment',
            'getusercomment',
            'getcommentlikecount',
            'getuserlike',
            'getcommentusercount'
        ]);
        $actions = [
            'postcomment',
            'getcomment',
            'likecomment',
            'getusercomment',
            'getcommentlikecount',
            'getuserlike',
            'getcommentusercount'
        ];

        if (in_array($this->request->getParam('action'), $actions)) {
            $this->eventManager()->off($this->Csrf);
            $this->Security->config('unlockedActions', $actions);
        }
    }
    
	public function index(){}

	public function add()
	{
		$communication = $this->Communications->newEntity();
		if ($this->request->is('post')) {
			$communication = $this->Communications->patchEntity($communication, $this->request->getData());
			if ($this->Communications->save($communication)) {
				$this->autoRender = false;
				echo json_encode(['result' => 'success', 'communication_data' => $this->request->getData()]);
			} else {
				echo json_encode(['result' => 'error']);
			}
		}
		$this->set(compact('communication'));
	}

    public function edit($id = null, $app_id = null)
    {
        $communication = $this->Communications->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                $this->autoRender = false;
                echo json_encode(['result' => 'success', 'communication_data' => $this->request->getData()]);
            } else {
                echo json_encode(['result' => 'error']);
            }
        }
        $this->set(compact('communication','app_id'));
    }

    public function delete($id = null, $app_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communication = $this->Communications->get($id);
        if ($this->Communications->delete($communication)) {
            $this->Flash->success(__('Successfully removed.'));
        } else {
            $this->Flash->error(__('The communication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'lists', $app_id]);
    }

	public function lists($id = null) 
    {
        $communication = $this->Communications->newEntity();
        if ($this->request->is('post')) {
            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                $this->autoRender = false;
                echo json_encode(['result' => 'success', 'communication_data' => $this->request->getData()]);
            } else {
                echo json_encode(['result' => 'error']);
            }
        }
		$communicationlist = $this->Communications->find()->where(['application' => $id, 'company' => $this->Auth->user('company')]);
		$this->set(compact('communicationlist','id','communication'));
	}

	public function archived($id = null) 
    {
		$communicationlist = $this->Communications->find()->where(['status' => 3, 'application' => $id, 'company' => $this->Auth->user('company')]);
		$this->set(compact('communicationlist','id'));
	}

	public function postcomment() 
	{
		$this->autoRender = false;
		$comments = $this->Comments->newEntity();
		if ( $this->request->is('ajax') && $this->request->is('post')) {
			$comments = $this->Comments->patchEntity($comments, $this->request->getData());
			if ($this->Comments->save($comments)) {
				echo json_encode(['result' => 'success', 'comments_data' => $this->request->getData()]);
			} else {
				echo json_encode(['result' => 'error']);
			}
		}
	}

	public function likecomment() 
	{
		$this->autoRender = false;
		$likes = $this->Likes->newEntity();
		$likeTable = TableRegistry::get('Likes');
		$exist = $likeTable->find()->where(['users' => $this->request->getData('users'), 'communication_id' => $this->request->getData('communication_id')])->count();
		if($exist > 0) {
			$removedExist = $likeTable->query()->delete()->where(['users' => $this->request->getData('users'), 'communication_id' => $this->request->getData('communication_id')])->execute();
			if ($removedExist) {
				echo json_encode(['result' => 'remove_exist']);
			}
		} else {
			if ( $this->request->is('ajax') && $this->request->is('post')) {
				$likes = $this->Likes->patchEntity($likes, $this->request->getData());
				if ($this->Likes->save($likes)) {
					echo json_encode(['result' => 'success', 'likes_data' => $this->request->getData()]);
				} else {
					echo json_encode(['result' => 'error']);
				}
			}
		}
	}


	public function getcomment() 
	{
		$this->autoRender = false;
		if ( $this->request->is('ajax') && $this->request->is('post')) {
			$data = $this->request->getData();
			$comments = $this->Comments->find('all')->contain(['Users'])->where(['communication_id' => $data['communication_id']])->order(['post_date' => 'DESC']);
			echo json_encode(['comments_data' => $comments]);
		}
	}

	public function getusercomment() 
	{
		$this->autoRender = false;
		if ( $this->request->is('ajax') && $this->request->is('post')) {
			$data = $this->request->getData();
			$comments = $this->Comments->find('all')->contain(['Users'])->where(['communication_id' => $data['communication_id']])->order(['post_date' => 'DESC'])->group(['users']);
			echo json_encode(['comments_data' => $comments]);
		}
	}

	public function getuserlike() 
	{
		$this->autoRender = false;
		if ( $this->request->is('ajax') && $this->request->is('post')) {
			$data = $this->request->getData();
			$likes = $this->Likes->find('all')->contain(['Users'])->where(['communication_id' => $data['communication_id']])->order(['date_post' => 'DESC'])->group(['users']);
			echo json_encode(['user_like_data' => $likes]);
		}
	}

	public function usercomments($id = null, $list_id = null)
	{
		$usercomments = $this->Comments->find('all')->contain(['Users'])->where(['communication_id' => $id])->order(['post_date' => 'DESC']);
		$this->set(compact('usercomments','id','list_id'));
	}

	public function userlikes($id = null, $list_id = null)
	{
		$userlikes = $this->Likes->find('all')->contain(['Users'])->where(['communication_id' => $id])->order(['date_post' => 'DESC']);
		$this->set(compact('userlikes','id','list_id'));
	}

	public function getcommentlikecount($id = null) {
		$this->autoRender = false;
		$query = $this->Likes->find()->where(['communication_id' => $id])->count();
		echo json_encode(['count' => $query]);
	}

	public function getcommentusercount($id = null) {
		$this->autoRender = false;
		$query = $this->Comments->find()->where(['communication_id' => $id]);
		echo json_encode(['count' => $query->count()]);
	}

    public function toarchived($id = null, $app_id = null)
    {
        $this->autoRender = false;
        $communicationTable = TableRegistry::get('Communications');
        $communication = $communicationTable->get($id);

        $communication->status = 3;
        $communicationTable->save($communication);
        $this->Flash->success(__('Successfully move to archived.'));
        return $this->redirect(['action' => 'lists', $app_id]);
    }

    public function updatestatus($id = null, $status = null, $app_id = null)
    {
        $commsTable = TableRegistry::get('Communications');
        $comms = $commsTable->get($id);
        $comms->status = $status;
        if($commsTable->save($comms)) {
            $this->Flash->success(__('Updated Status.'));
            return $this->redirect(['action' => 'lists',$app_id]);
        }
    }

	public function acknowledge($id = null, $app_id = null)
	{
		$acknowledges = $this->Acknowledges->find('all')->contain(['Users'])->where(['communication_id' => $id])->toArray();
		$this->set(compact('acknowledges','app_id'));
	}

    public function modaldata() {
		$this->autoRender = false ;   
		$data = $this->request->getData();
		$communication = $this->Communications->get($data['id']);
		$json_data = json_encode(
			array(
				'communication' => $communication,
				'request' => isset($data['title'])? $data['title'] : ''
			)
		);
		$response = $this->response->withType('json')->withStringBody($json_data);
		return $response;
    }
}
