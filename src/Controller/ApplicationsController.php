<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ApplicationsController extends AppController
{

	public function initialize(){
		parent::initialize();

        $this->Auth->allow([
            'index',
            'applicationlist',
            'add',
            'edit',
            'delete',
            'acknowledge',
            'company',
            'assigncompany',
            'dashboard'
        ]);
		$this->loadComponent('Paginator');
		$this->viewBuilder()->setLayout('home');
	}

	public function index()
	{
		$applications = $this->paginate($this->Applications);
		$this->set(compact('applications'));
	}

	public function applicationlist()
	{
		$this->set('page_title', 'DASHBOARD');

		$applications = TableRegistry::get('Applications')->find();
		$settings = ['limit' => 1,'maxLimit' => 100];
		$communicationlist = $this->paginate($this->Communications->find()->where(['acknowledge_status' => 1]), $settings);

		$this->set('applications', $applications);
		$this->set(compact('communicationlist'));
	}

	public function dashboard() {
		$this->set('page_title', 'DASHBOARD');
		if($this->Auth->user('role') == "Admin") {
			return $this->redirect(['controller' => 'Companies','action' => 'index']);
		}

		$applications = TableRegistry::get('Applications')->find();
		$settings = ['limit' => 1,'maxLimit' => 100];
		$communicationlist = $this->paginate($this->Communications->find()->where([
			'status' => 2,
			'company' => $this->Auth->user('company'),
			'acknowledge not LIKE' => '%'.$this->Auth->user('id').'%'
		]), $settings);
		$applications = TableRegistry::get('Applications')->find()->where(['company LIKE' => '%'.$this->Auth->user('company').'%']);
		$user_id = $this->Auth->user('id');
		$this->set(compact('communicationlist','applications','user_id'));
	}

	public function company($id = null)
	{
		$applications = $this->Applications->find('all')->where(['id' => $id])->toArray();
		$applications_company = $application_name = '';
		foreach ($applications as $key => $application) {
			$applications_company = $application->company;
			$application_name = $application->name;
		}
		$companies = $this->Companies->find('all')->where(['id IN' => array_map('intval', explode(',', $applications_company))]);
		$unassign_company = $this->Companies->find('list')->where(['id NOT IN' => array_map('intval', explode(',', $applications_company))])->toArray();
		$this->set(compact('companies','application_name','unassign_company','applications_company','id'));
	}

	public function add()
	{
		$application = $this->Applications->newEntity();
		if ($this->request->is('post')) {
			if(!empty($this->request->data['icon']['name'])){
				$fileName = $this->request->data['icon']['name'].''.time();
				$uploadPath = 'widgets/';
				$uploadFile = $uploadPath.$fileName;
				if(move_uploaded_file($this->request->data['icon']['tmp_name'],$uploadFile)){
					$this->request->data['icon'] = $uploadFile;
				}
			}
			$application = $this->Applications->patchEntity($application, $this->request->getData());
			if ($this->Applications->save($application)) {
				$this->Flash->success(__('The application has been saved.'));

				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('The application could not be saved. Please, try again.'));
		}
		$this->set(compact('application'));
	}

	public function assigncompany()
	{
		if($this->request->is(['post','put'])) {
			$data = $this->request->getData();
			$applicationsTable = TableRegistry::get('Applications');
			$application = $applicationsTable->get($data['id']);
			$data_company = '';
			if(!empty($data['company'])) {
				$data_company = $data['company'].',';
			}

			$application->company = $data_company.''.implode(',', $data['company_select']);

			if($applicationsTable->save($application)) {
				$this->Flash->success(__('Assign Company Successfully'));
		 		return $this->redirect(['action' => 'company',$data['id']]);
			}
			$this->Flash->error(__('Failed Assign.'));
		}
	}

    public function edit($id = null)
    {
        $application = $this->Applications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	if(!empty($this->request->data['icon']['name'])){
				$fileName = $this->request->data['icon']['name'];
				$uploadPath = 'widgets/'.time();
				$uploadFile = $uploadPath.$fileName;
				if(move_uploaded_file($this->request->data['icon']['tmp_name'],$uploadFile)){
					$this->request->data['icon'] = $uploadFile;
				}
			}
            $application = $this->Applications->patchEntity($application, $this->request->getData());
            if ($this->Applications->save($application)) {
                $this->Flash->success(__('The application has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The application could not be saved. Please, try again.'));
        }
        $this->set(compact('application'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $application = $this->Applications->get($id);
        if ($this->Applications->delete($application)) {
            $this->Flash->success(__('The application has been deleted.'));
        } else {
            $this->Flash->error(__('The application could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function acknowledge()
	{
		$this->autoRender = false;
		$acknowledges = $this->Acknowledges->newEntity();
		if ($this->request->is('post')) {
			$data = $this->request->getData();
			$commTable = TableRegistry::get('Communications');
			$comm = $commTable->get($data['communication_id']);
			$application = $this->Acknowledges->patchEntity($acknowledges, $this->request->getData());
			$acknowledged = $data['acknowledged'];
			$user_acknowledged = '';
			if(!empty($acknowledged)) {
				$user_acknowledged = $acknowledged.','.$this->Auth->user('id');
			} else {
				$user_acknowledged = $this->Auth->user('id');
			}
			$comm->acknowledge = $user_acknowledged;
			if ($this->Acknowledges->save($acknowledges) && $commTable->save($comm) ) {
				$this->Flash->success(__('The communication has been acknowledged.'));

				return $this->redirect(['action' => 'dashboard']);
			}
			$this->Flash->error(__('The communication could not be acknowledge. Please, try again.'));
		}
	}
}
