<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->Auth->allow([
			'logout',
			'add',
			'index',
			'login',
			'view',
			'delete',
			'edit'
		]);

        $role = $this->Obj->fta('Roles');
        $company = $this->Obj->selection('Companies');
        $this->set(compact('role','company'));
	}

    public function index()
    {
		$this->viewBuilder()->setLayout('home');
        $users = $this->Users->find();
        if($this->Auth->user('role') == "User") {
            $users = $this->Users->find()->where(['company' => $this->Auth->user('company')]);
        }
        
        $this->set('users',$users);
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Bookmarks']
        ]);

        $this->set('user', $user);
    }

	public function login()
	{
		$this->viewBuilder()->getLayout('default');
		if($this->Auth->user('id')) { 
			$this->Flash->warning(__('You are already logged in!'));
			return $this->redirect($this->Auth->redirectUrl());
		}
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				$this->Flash->success(__('Login Successful'));
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error('Your username or password is incorrect.');
		}
		
	}

	public function logout()
	{
		$this->Flash->success('You are now logged out.');
		return $this->redirect($this->Auth->logout());
	}

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->autoRender = false;
                echo json_encode(['result' => 'success', 'data' => $this->request->getData()]);
            } else {
                echo json_encode(['result' => 'error']);
            }
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
    	$this->viewBuilder()->setLayout('home');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
