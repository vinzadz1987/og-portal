<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property string $users
 * @property int $communication_id
 * @property string $comment
 * @property string $post_date
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Communication $communication
 */
class Comment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'users' => true,
        'communication_id' => true,
        'comment' => true,
        'post_date' => true,
        'created' => true,
        'communication' => true
    ];
}
