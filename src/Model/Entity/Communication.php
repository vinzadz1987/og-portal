<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Communication Entity
 *
 * @property int $id
 * @property bool $commtype
 * @property string $author
 * @property \Cake\I18n\FrozenDate $start_date
 * @property \Cake\I18n\FrozenDate $stop_date
 * @property string $heading
 * @property string $message
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Communication extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        // 'communication_type' => true,
        'author' => true,
        'start_date' => true,
        'stop_date' => true,
        'heading' => true,
        'message' => true,
        'application' => true,
        'company' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
