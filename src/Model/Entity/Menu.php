<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Menu Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $sub
 * @property string $controller
 * @property string $action
 * @property string $icon
 * @property \Cake\I18n\FrozenTime $created
 */
class Menu extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'sub' => true,
        'controller' => true,
        'action' => true,
        'icon' => true,
        'created' => true
    ];
}
