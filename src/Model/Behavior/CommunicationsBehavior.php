<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

/**
 * Communications behavior
 */
class CommunicationsBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public $actsAs = array('WebSocket.Publishable' => array('fields' => array('name', 'status_date', 'status_code', 'status_progress')),
}
