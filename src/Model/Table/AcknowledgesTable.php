<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AcknowledgesTable extends Table
{

	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('acknowledges');
		$this->setDisplayField('id');
		$this->setPrimaryKey('id');

		$this->addBehavior('Timestamp');

		$this->belongsTo('Communications', [
			'foreignKey' => 'communication_id',
			'joinType' => 'INNER'
		]);

		$this->belongsTo('Users', [
			'foreignKey' => 'users',
			'joinType' => 'INNER'
		]);
	}

	public function validationDefault(Validator $validator)
	{
		$validator
			->integer('id')
			->allowEmpty('id', 'create');

		$validator
			->integer('users')
			->requirePresence('users', 'create')
			->notEmpty('users');

		$validator
			->integer('company')
			->requirePresence('company', 'create')
			->notEmpty('company');

		return $validator;
	}

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['communication_id'], 'Communications'));

        return $rules;
    }
}
