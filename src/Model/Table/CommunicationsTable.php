<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Communications Model
 *
 * @method \App\Model\Entity\Communication get($primaryKey, $options = [])
 * @method \App\Model\Entity\Communication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Communication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Communication|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Communication|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Communication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Communication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Communication findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('communications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

		$this->belongsTo('Comments', [
			'foreignKey' => 'id',
			'joinType' => 'INNER'
		]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->boolean('communication_type')
        //     ->requirePresence('communication_type', 'create')
        //     ->notEmpty('communication_type');

        $validator
            ->scalar('author')
            ->maxLength('author', 500)
            ->requirePresence('author', 'create')
            ->notEmpty('author');

        $validator
            ->date('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->date('stop_date')
            ->allowEmpty('stop_date');

        $validator
            ->scalar('heading')
            ->maxLength('heading', 500)
            ->requirePresence('heading', 'create')
            ->notEmpty('heading');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->integer('application')
            ->notEmpty('application','create');

         $validator
            ->integer('company')
            ->notEmpty('company','create');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }
}
