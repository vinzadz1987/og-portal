var Connection = (function(){

  function Connection(url) {

    this.open = false;

    this.socket = new WebSocket("ws://" + url);
    this.setupConnectionEvents();
  }

  Connection.prototype = {
    setupConnectionEvents : function () {
      var self = this;

      self.socket.onopen = function(evt) { self.connectionOpen(evt); };
      self.socket.onmessage = function(evt) { self.connectionMessage(evt); };
      self.socket.onclose = function(evt) { self.connectionClose(evt); };
    },

    connectionOpen : function(evt){
      this.open = true;
    },

    connectionMessage : function(evt){
      var data = JSON.parse(evt.data);
      this.addCommunication(data.msg);
    },

    connectionClose : function(evt){
      this.open = false;
    },

    sendDataCommunication : function(message){
      try {
        this.socket.send(JSON.stringify({
          msg : message
        }));
      } catch(e) {

      }
    },

    addCommunication : function(data){
      switch(data.broadType){
        case Broadcast.POST : this.addCommunicationPost(data); break;
        default : console.log("nothing to do");
      }
    },

	addCommunicationPost : function(object){
		var commData = object.data;
		console.log(commData);
		var commuContent = '<tr>'+
			'<td>'+commData.heading+'</td>'+
			'<td>'+commData.author+'</td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
			'<td></td>'+
		'</tr>';
		$("#communication_table tbody").prepend(commuContent);
	},

  };

  return Connection;

})();
