'use strict';
$(function () {
	
	$(".addBtn").click( function() {
		$("#communication_list").addClass("col-sm-8");
		$(".addCommDiv").removeClass('hidden');
	});
	$(".cancelAdd").click( function () {
		$("#communication_list").removeAttr("class");
		$("#communication_list").attr("class",'col-sm-12');
		$(".addCommDiv").addClass('hidden');
	});

	$("button[name='save']").on('click', function() {
		$("#status").val('1');
	});
	$("button[name='publish']").on('click', function() {
		$("#status").val('2');
	});

	$('#communication_table tbody').on('click', '.postComment', function () {
		postComment($(this));
	});
	$('#communication_table tbody').on('click', '.userComment', function () {
		showUserComment($(this));
	});

	$('#communication_table tbody').on('click', '.likeComment', function () {
		likeComment($(this));
	});

	$('#communication_table tbody').on('click', '.commTitle', function () {
		getData($(this).data('id'));
	});

	$.validator.setDefaults( {
		submitHandler: function () {
			$.ajax({
				type: 'post',
				async: true,
				cache: false,
				url:'',
				success: function (response) {
					var pj = $.parseJSON(response);
					if(pj.result == "success") {
						var typeData = { broadType : Broadcast.POST, data : pj.communication_data };
						connection.sendDataCommunication(typeData);
						toastr.success('Added communication succesfully.','Success');
						$("#status").val('');
						$("form#frmAddCommu")[0].reset();
					} else {
						toastr.error('Something error in adding communication.', 'Error!');
					}
				},
				error: function(response) {
					toastr.error('Something error in adding communication.', 'Error!');
				},
				data: $("form#frmAddCommu").serialize()
			});
		}
	} );

	$( "#frmAddCommu" ).validate( {
		rules: {
			author: "required",
			start_date: "required",
			stop_date: "required",
			end_date: 'required',
			heading: 'required',
			messages: "required"
		},
		messages: {
			author: "Please enter author",
			start_date: "Please enter start date",
			stop_date: "Please enter stop date",
			heading: 'Please enter heading',
			message: "Please enter a message"
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			error.addClass( "help-block" );
			element.parents( ".col-sm-5" ).addClass( "has-feedback" );

			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}

			if ( !element.next( "span" )[ 0 ] ) {
				$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
			}
		},
		success: function ( label, element ) {
			if ( !$( element ).next( "span" )[ 0 ] ) {
				$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
		},
		unhighlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
		}
	} );


});

function getData(id) {
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/getcommentlikecount/'+id,
		success: function (response) {
			var countLike = $.parseJSON(response);
			$('#communication_table tbody').find('.likeCount'+id).text(countLike['count']);
		},
		error: function(response) {
			toastr.error('Something error in counting like.', 'Error!');
		},
	});

	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/getcommentusercount/'+id,
		success: function (response) {
			var countUser = $.parseJSON(response);
			$('#communication_table tbody').find('.countCommentUser'+id).text(countUser['count']);
		},
		error: function(response) {
			toastr.error('Something error in counting like.', 'Error!');
		},
	});
}

function showComment(param, ident) {
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/getcomment',
		success: function (response) {
			var comments = $.parseJSON(response);
			var commentPop = '';
			var countComment = 0;
			$.each( comments.comments_data, function( key, value) {
				commentPop += '<div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="2b0bcd3b-9b73-1744-2a81-fb741ce83e84">'+
					'<div class="d-flex flex-row comment-row m-t-0">'+
						'<div class="p-2"><img src="http://svgur.com/i/65U.svg" alt="user" width="50" class="rounded-circle"></div>'+
							'<div class="comment-text w-100">'+
							'<h6 class="font-medium">'+value.user.firstname+' '+value.user.lastname+'</h6>'+
							'<span class="m-b-15 d-block">'+value.comment+' <small class="text-muted float-right">'+value.post_date+'</small></span>'+
						'</div>'+
					'</div>'+
				'</div>';
				countComment++;
			});
			$(".countComment"+$(param).data('id')).html(countComment);
			$("#commentHere"+$(param).data('id')).html(commentPop);
		},
		error: function(response) {
			toastr.error('Something error in showing comments.', 'Error!');
		},
		data: { communication_id: $(param).data('id') }
	});

	if(ident == 2) {
		$('#commentBox'+$(param).data('id')).show();
		$('#commentHere'+$(param).data('id')).removeClass('disno');
		$('#commentBox'+$(param).data('id')).find('textarea').focus();
		$('.commentBtn'+$(param).data('id')).attr('onclick',"hideComment("+$(param).data('id')+")");
		if(!$('#view_content'+$(param).data('id')).hasClass('disno')) {
			$("#view_content"+$(param).data('id')).addClass('disno');
		}
	} 
}

function hideComment(id) 
{
	$('#commentBox'+id).hide();
	$('#commentHere'+id).addClass('disno');
	$('.commentBtn'+id).attr('onclick',"showComment(this, 2)");
	if(!$('#view_content'+id).hasClass('disno')) {
		$("#view_content"+id).addClass('disno');
	}
}

function postComment(objects) {
	var dateObj = new Date();
	var month = dateObj.getUTCMonth() + 1;
	var day = dateObj.getUTCDate();
	var year = dateObj.getUTCFullYear();
	var postDate = year + "-" + month + "-" + day;
	var time = dateObj.getHours() + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/postcomment',
		success: function (response) {
			var comments = $.parseJSON(response);
			if(comments.result == "success") {
				var commentPop = '<div class="comment-widgets scrollable ps-container ps-theme-default" data-ps-id="2b0bcd3b-9b73-1744-2a81-fb741ce83e84">'+
					'<div class="d-flex flex-row comment-row m-t-0">'+
						'<div class="p-2"><img src="http://svgur.com/i/65U.svg" alt="user" width="50" class="rounded-circle"></div>'+
							'<div class="comment-text w-100">'+
							'<h6 class="font-medium">'+objects.prev().prev().val()+'</h6>'+
							'<span class="m-b-15 d-block">'+comments.comments_data.comment+'</span>'+
							'<div class="comment-footer">'+
								'<span class="text-muted float-right">'+postDate+' '+time+'</span>'+ 
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
				$("#commentHere"+objects.data('id')).prepend(commentPop);
				objects.prev('textarea').val('');	
				var commentCount = (parseInt($(".countComment"+objects.data('id')).text()) + 1);
				$(".countComment"+objects.data('id')).html(commentCount);
				$('#commentBox'+objects.data('id')).find('textarea').focus();
				toastr.success('comment added successfully');
			}
		},
		error: function(response) {
			toastr.error('Something error in adding comments.', 'Error!');
		},
		data: { communication_id: objects.data('id'), comment: objects.prev().val().trim(), users: objects.prev().prev().prev().val(), post_date:postDate+' '+time }
	});		
}

function likeComment(obj) {
	var dateObj = new Date();
	var month = dateObj.getUTCMonth() + 1;
	var day = dateObj.getUTCDate();
	var year = dateObj.getUTCFullYear();
	var postDate = year + "-" + month + "-" + day;
	var time = dateObj.getHours() + ":" + dateObj.getMinutes() + ":" + dateObj.getSeconds();
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/likecomment',
		success: function (response) {
			var response = $.parseJSON(response);
			var prevCount = $('#communication_table tbody').find('.likeCount'+obj.data('id')).html();
			var countComment = '';
			if(response.result == "remove_exist") {
				countComment = (parseInt(prevCount) - 1);
				toastr.success('You unliked.');
			} else {
				countComment = (parseInt(prevCount) + 1);
				toastr.success('You liked.');
			}
			$('#communication_table tbody').find('.likeCount'+obj.data('id')).text(countComment);
		},
		error: function(response) {
			toastr.error('Something error in liking comment.', 'Error!');
		},
		data: { communication_id: obj.data('id'), users: obj.data('user'), date_post: postDate+' '+time }
	});		
}

function showUserComment(param) 
{
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/getcomment',
		success: function (response) {
			$("#view_content"+$(param).data('id')).addClass('loadingAnimated');
			var puser = $.parseJSON(response);
			var puserArray = '<div class="l10"><h4 style="padding-left: 10px;">Comment Users</h4></div>'+
				'<div class="row col-md-12" style="padding: 20px; margin-left: 0px;">';
			$.each(puser.comments_data, function( key, value) {
				puserArray+='<div class="col-md-4 p5 table-bordered text-center">'+value.user.firstname+' '+value.user.lastname+'</div>'+
							  '<div class="col-md-4 p5 table-bordered text-center">'+value.comment+' </div>'+
							  '<div class="col-md-4 p5 table-bordered text-center">'+value.post_date+'</div>';
			});
			if(puserArray == '') {
				puserArray = '<span class="text-center"><b>No Record Found</b></span>';
			}
			$("#view_content"+$(param).data('id')).html(puserArray+'</div>');
			$("#view_content"+$(param).data('id')).removeClass('loadingAnimated');
		},
		error: function(response) {
			toastr.error('Something error in show user comments.', 'Error!');
		},
		data: { communication_id: $(param).data('id') }
	});
	if($('#view_content'+$(param).data('id')).hasClass('disno')) {
		$("#view_content"+$(param).data('id')).removeClass('disno');
	}
	if(!$('#commentBox'+$(param).data('id')).hasClass('disno')) {
		$('#commentBox'+$(param).data('id')).hide();
		$('#commentHere'+$(param).data('id')).addClass('disno');
		$('.commentBtn'+$(param).data('id')).attr('onclick',"showComment(this, 2)");
	}
}

function showUserLike(param) 
{
	$.ajax({
		type: 'post',
		async: true,
		cache: false,
		url:'/og-portal/communications/getuserlike',
		success: function (response) {
			var puser = $.parseJSON(response);
			var puserArray = '<div class="l10"><h4 style="padding-left: 10px;">Users Like</h4></div>'+
				'<div class="row col-md-12" style="padding: 20px; margin-left: 0px;">';
			$.each(puser.user_like_data, function( key, value) {
				puserArray+='<div class="col-md-4 p5 table-bordered text-center"><img src="http://svgur.com/i/65U.svg" alt="user" class="rounded-circle" style="width:30px"></div>'+
							  '<div class="col-md-4 p5 table-bordered text-center">'+value.user.firstname+' '+value.user.lastname+' </div>'+
							  '<div class="col-md-4 p5 table-bordered text-center">'+value.date_post+'</div>';
			});
			if(puserArray == '') {
				puserArray = '<span class="text-center"><b>No Record Found</b></span>';
			}
			$("#view_content"+$(param).data('id')).html(puserArray+'</div>');
			$("#view_content"+$(param).data('id')).removeClass('loadingAnimated');
		},
		error: function(response) {
			toastr.error('Something error in show user likes.', 'Error!');
		},
		data: { communication_id: $(param).data('id') }
	});
	if($('#view_content'+$(param).data('id')).hasClass('disno')) {
		$("#view_content"+$(param).data('id')).removeClass('disno');
	}
	if(!$('#commentBox'+$(param).data('id')).hasClass('disno')) {
		$('#commentBox'+$(param).data('id')).hide();
		$('#commentHere'+$(param).data('id')).addClass('disno');
		$('.commentBtn'+$(param).data('id')).attr('onclick',"showComment(this, 2)");
	}
}