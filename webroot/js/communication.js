$(function() {
	'use strict';

	$('.popper').popover({
		container: 'body',
		html: true,
		content: function () {
			return $(this).next('.popper-content').html();
		}
	});

	/****************************************
	*       Popup communication             *
	****************************************/

	$(".checkAcknowledge").change( function () {
		$(".btnAcknowledge"+$(this).data('id')).show().prev().hide();
	});

	$(".acknowledgeme").click( function() {
		$("#acknowledid").val($(this).data('id'));
		toastr.success('Acknowledge Successfull.','Success');
		window.setTimeout( function () {
			$("#acknowledge").submit();
		}, 1000);
	});


	/****************************************
	*       End Popup communication             *
	****************************************/

	/****************************************
	*       Sortable Widgets                *
	****************************************/
	$( "#sortable" ).sortable({
		update: function (event, ui) {
			saveDrag();
		}
	});
	$( "#sortable" ).disableSelection();
	$( document ).tooltip({
		position: {
			my: "center bottom-20",
			at: "center top",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
				.addClass( "arrow" )
				.addClass( feedback.vertical )
				.addClass( feedback.horizontal )
				.appendTo( this );
			}
		}
	});
	/****************************************
	*       End Sortable Widgets            *
	****************************************/

	/****************************************
	*       Basic Table                   *
	****************************************/

	$('#start-date, #stop-date').datepicker(
	{	
		changeYear: true,
		changeMonth: true,
		dateFormat: "yy-mm-dd",
		minDate: 0,
		setDate: new Date()
	});
	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();
	$( document ).tooltip({
		position: {
			my: "center bottom-20",
			at: "center top",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
				.addClass( "arrow" )
				.addClass( feedback.vertical )
				.addClass( feedback.horizontal )
				.appendTo( this );
			}
		}
	});

	/****************************************
	*       End Widget Table                *
	****************************************/
	
   var table = $('#widgetList').DataTable({});

	/****************************************
	*    End Widget Table                   *
	****************************************/

});

// drag widgets	
function saveDrag() 
{
	var theListArray = [];
	$("li", "#sortable").each( function (count, item) {
		theListArray[count] = $(this).text();
	});
	var appList = theListArray.toString();
}	

// trigge modal communication
function triggerModalData(id, title) {
	$("#view_content").attr('class','loadingAnimated').html('');
	$.ajax({
		type: 'POST',
		url: 'modaldata',
		data: {id: id, title: title},
		dataType: 'json',
		success: function (response) {
			$("#view_title").html(response.request);
			$("#view_content").html(response.communication.message).removeAttr('class');
		},
		error: function () {
			alert('error');
		}
	});
}

