$(function() {
	$(".addUserBtn").click( function() {
		$("#usersList").addClass("col-sm-8");
		$(".addUserDiv").removeClass('hidden');
	});
	$(".cancelAddUser").click( function () {
		$("#usersList").removeAttr("class");
		$("#usersList").attr("class",'col-sm-12');
		$(".addUserDiv").addClass('hidden');
		$("form#frmAddUser")[0].reset();
	});
	$(".updCompRole").click( function() {
		console.log($(this).data('id'));
		console.log($(this).data('role'));
		if($(this).data('role') == 1) {
			company_role = 0;
		} else {
			company_role = 1;
		}
		console.log("<?= BASE_URL ?>");
		$.ajax({
			type: 'post',
			async: true,
			cache: false,
			url:'companies/addmin',
			success: function (response) {
				console.log(response);
				// var pj = $.parseJSON(response);
				// if(pj.result == "success") {
				// 	toastr.success('Added user succesfully.','Success');
				// 	$("form#frmAddUser")[0].reset();
				// } else {
				// 	toastr.error('Something error in adding communication.', 'Error!');
				// }
			},
			error: function(response) {
				toastr.error('Something error in changing access.', 'Error!');
			},
			data: { id: $(this).data('id'), company_role: company_role}
		});
	});
	$.validator.setDefaults( {
			submitHandler: function () {
				$.ajax({
					type: 'post',
					async: true,
					cache: false,
					url:'users/add',
					success: function (response) {
						var pj = $.parseJSON(response);
						if(pj.result == "success") {
							toastr.success('Added user succesfully.','Success');
							$("form#frmAddUser")[0].reset();
						} else {
							toastr.error('Something error in adding communication.', 'Error!');
						}
					},
					error: function(response) {
						toastr.error('Something error in adding communication.', 'Error!');
					},
					data: $("form#frmAddUser").serialize()
				});
			}
		} );

		$( "#frmAddUser" ).validate( {
				rules: {
					firstname: "required",
					lastname: "required",
					password: {
						required: true,
						minlength: 5
					},
					email: {
						required: true,
						email: true
					},
					company: "required"
				},
				messages: {
					firstname: "Please enter your firstname",
					lastname: "Please enter your lastname",
					password: {
						required: "Please provide a password",
						minlength: "Your password must be at least 5 characters long"
					},
					email: "Please enter a valid email address"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-5" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
		} );
});
