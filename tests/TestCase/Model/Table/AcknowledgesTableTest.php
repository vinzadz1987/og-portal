<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcknowledgesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcknowledgesTable Test Case
 */
class AcknowledgesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AcknowledgesTable
     */
    public $Acknowledges;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.acknowledges',
        'app.communications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Acknowledges') ? [] : ['className' => AcknowledgesTable::class];
        $this->Acknowledges = TableRegistry::getTableLocator()->get('Acknowledges', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Acknowledges);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
