<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubmenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubmenusTable Test Case
 */
class SubmenusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubmenusTable
     */
    public $Submenus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.submenus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Submenus') ? [] : ['className' => SubmenusTable::class];
        $this->Submenus = TableRegistry::getTableLocator()->get('Submenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Submenus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
